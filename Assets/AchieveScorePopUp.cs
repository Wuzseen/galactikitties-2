﻿using UnityEngine;
using System.Collections;

public class AchieveScorePopUp : MonoBehaviour {
	public UILabel buttonLabel;
	public string loadedScreen;
	public GameObject achievements;
	public UILabel screenLabel;
	public GameObject highScoreGroup;
	// Use this for initialization
	void Start () {
		loadedScreen = "Achievements";
		highScoreGroup.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnClick(){
		if(loadedScreen == "Achievements"){
			loadedScreen = "High Scores";
		}
		else if(loadedScreen == "High Scores"){
			loadedScreen = "Achievements";
		}
		ChangeScreen ();
	}

	void ChangeScreen(){
		screenLabel.text = loadedScreen;
		if(loadedScreen == "Achievements"){
			buttonLabel.text = "High Scores";
			highScoreGroup.SetActive(false);
			achievements.SetActive(true);
		}
		else if(loadedScreen == "High Scores"){
			buttonLabel.text = "Achievements";
			achievements.SetActive(false);
			highScoreGroup.SetActive(true);
		}
	}
}
