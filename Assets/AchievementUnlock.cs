﻿using UnityEngine;
using System.Collections;

public class AchievementUnlock : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void ClearPlayerPrefs() {
		Achievements.Instance.ClearPrefsAndAchievements();
	}

	void OnClick(){
		Achievements.Instance.UnlockAll();
	}
}
