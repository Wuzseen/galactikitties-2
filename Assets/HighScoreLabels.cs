﻿using UnityEngine;
using System.Collections;

public class HighScoreLabels : MonoBehaviour {

	public UILabel[] scoreLabels = new UILabel[5];
	public UILabel gameType;

	// Use this for initialization
	void Start () {
		string[] labels = PlayerPrefs.GetString("_classicScores").Split(',');
		for(int i = 0; i < labels.Length; i++){
			gameType.text = "Normal";
			if(labels[i] == ""){
				scoreLabels[i].text = "0";
			}else {
				scoreLabels[i].text = labels[i];
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void getNextScoreType(){
		if(gameType.text == "Normal"){
			gameType.text = "Time Attack";
			string[] labels = PlayerPrefs.GetString("_timeScores").Split(',');
			for(int i = 0; i < labels.Length; i++){
				if(labels[i] == ""){
					scoreLabels[i].text = "0";
				}else {
					scoreLabels[i].text = labels[i];
				}
			}
		}
		else if(gameType.text == "Time Attack"){
			gameType.text = "Survival";
			string[] labels = PlayerPrefs.GetString("_survivalScores").Split(',');
			for(int i = 0; i < labels.Length; i++){
				if(labels[i] == ""){
					scoreLabels[i].text = "0";
				}else {
					scoreLabels[i].text = labels[i];
				}
			}
		}
		else if(gameType.text == "Survival"){
			gameType.text = "Normal";
			string[] labels = PlayerPrefs.GetString("_classicScores").Split(',');
			for(int i = 0; i < labels.Length; i++){
				if(labels[i] == ""){
					scoreLabels[i].text = "0";
				}else {
					scoreLabels[i].text = labels[i];
				}
			}
		}
	}

	public void getPreviousScoreType(){
		if(gameType.text == "Survival"){
			gameType.text = "Time Attack";
			string[] labels = PlayerPrefs.GetString("_timeScores").Split(',');
			for(int i = 0; i < labels.Length; i++){
				if(labels[i] == ""){
					scoreLabels[i].text = "0";
				}else {
					scoreLabels[i].text = labels[i];
				}
			}
		}
		else if(gameType.text == "Normal"){
			gameType.text = "Survival";
			string[] labels = PlayerPrefs.GetString("_survivalScores").Split(',');
			for(int i = 0; i < labels.Length; i++){
				if(labels[i] == ""){
					scoreLabels[i].text = "0";
				}else {
					scoreLabels[i].text = labels[i];
				}
			}
		}
		else if(gameType.text == "Time Attack"){
			gameType.text = "Normal";
			string[] labels = PlayerPrefs.GetString("_classicScores").Split(',');
			for(int i = 0; i < labels.Length; i++){
				if(labels[i] == ""){
					scoreLabels[i].text = "0";
				}else {
					scoreLabels[i].text = labels[i];
				}
			}
		}
	}
}
