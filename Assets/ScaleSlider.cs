﻿using UnityEngine;
using System.Collections;

public class ScaleSlider : MonoBehaviour {
	
	private float baseaspect = 1.6f;
	// Use this for initialization
	void Start () {
		transform.localScale = new Vector3(1,1,1) * Camera.main.aspect/baseaspect;
	}
	
	// Update is called once per frame
	void Update () {
	}
}
