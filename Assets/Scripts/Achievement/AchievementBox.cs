﻿using UnityEngine;
using System.Collections;

public class AchievementBox : MonoBehaviour {
	public AchievementInfo boxInfo;
	public AchievementPanel pnl;
	public UILabel lbl;
	public UISprite fill;
	public UISprite bg;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void UpdateBox(AchievementInfo someInfo) {
		boxInfo = someInfo;
		lbl.text = boxInfo.Title;
		fill.fillAmount = Mathf.Clamp((float)boxInfo.Progress / (float)boxInfo.Total, 0f, 1f);
	}

	public void SetText(string someText) {
		lbl.text = someText;
	}

	public void OnSelect(bool selected) {
		pnl.SwapSelected(this);
	}
}
