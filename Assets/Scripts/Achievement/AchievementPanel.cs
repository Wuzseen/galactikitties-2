﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AchievementInfo {
	private string title;
	public string Title {
		set { title = value; }
		get { return title; }
	}
	
	private string info;
	public string Info {
		set { info = value; }
		get { return info; }
	}
	
	private int total;
	public int Total {
		set { total = value; }
		get { return total; }
	}
	
	private int progress;
	public int Progress {
		set { progress = value; }
		get { return progress; }
	}

	public bool IsUnlocked () {
		return progress >= total;
	}
}

public class AchievementPanel : MonoBehaviour {
	public UILabel achievementInfo;
	public UILabel achievementTitle;
	public UILabel achievementProgress;
	public UILabel achievementRequired;
	public Color selectedColor;
	private Color unselectedColor = Color.white;
	private AchievementBox currentlySelected;
	public GameObject achievementPrefab;
	private List<AchievementBox> achievements;
	private List<AchievementInfo> info;
	public int debugCount = 5;
	public int offset = 95;
	public float boxScale = 1.6f;
	public Transform startPoint;
	// Use this for initialization
	void Start () {
		achievements = new List<AchievementBox>();
		float x,y;
		x = startPoint.localPosition.x;
		y = startPoint.localPosition.y;
		if(Achievements.Instance == null) {		
			for(int i = 0; i < debugCount; i++) {
				Vector3 pos = new Vector3(x, y - offset * i, 0);
				GameObject achievementBox = (GameObject)Instantiate(achievementPrefab);
				achievementBox.transform.parent = this.transform;
				achievementBox.transform.localPosition = pos;
				achievementBox.transform.localScale = new Vector3(boxScale,boxScale,boxScale);
				achievements.Add(achievementBox.GetComponent<AchievementBox>());
				AchievementInfo someInfo = new AchievementInfo();
				someInfo.Info = string.Format("This is the {0:D}th achievement being displayed here, right now.  In this box.",i);
				someInfo.Title = string.Format("Achievement {0:D}",i);
				someInfo.Progress = 20*i;
				someInfo.Total = 100;
				achievements[i].UpdateBox(someInfo);
				achievements[i].pnl = this;
			}
		} else {
			Achievements.Instance.PropagatePlatinum();
			Dictionary<string,AchievementInfo> achievementInfo = Achievements.Instance.GetAllAchievementInfo();
			int i = 0;

			foreach(string key in achievementInfo.Keys) {
				Vector3 pos = new Vector3(x, y  - offset * i, 0);
				GameObject achievementBox = (GameObject)Instantiate(achievementPrefab);
				achievementBox.transform.parent = this.transform;
				achievementBox.transform.localPosition = pos;
				achievementBox.transform.localScale = new Vector3(boxScale,boxScale,boxScale);
				achievements.Add(achievementBox.GetComponent<AchievementBox>());
				achievements[i].UpdateBox(achievementInfo[key]);
				achievements[i].pnl = this;
				i++;
			}
		}

		SwapSelected(achievements[0]);
	}

	public void UpdateInfo(AchievementInfo someInfo) {
		achievementInfo.text = someInfo.Info;
		achievementTitle.text = someInfo.Title;
		achievementProgress.text = string.Format("Progress: {0:D}",someInfo.Progress);
		achievementRequired.text = string.Format("Required: {0:D}",someInfo.Total);
	}

	public void SwapSelected(AchievementBox newSelectedAchievement) {
		if(currentlySelected != null)
			currentlySelected.bg.color = unselectedColor;
		currentlySelected = newSelectedAchievement;
		currentlySelected.bg.color = selectedColor;
		UpdateInfo(newSelectedAchievement.boxInfo);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
