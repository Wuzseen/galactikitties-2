﻿using UnityEngine;
using System.Collections;

public class MuteButton : MonoBehaviour {
	private bool on = true; // WIll eventually load from a settings.xml...
	public UISprite icon;
	// Use this for initialization
	void Start () {
		if(PlayerPrefs.HasKey("volume")) {
			AudioListener.volume = PlayerPrefs.GetFloat("volume");
			if(AudioListener.volume > 0f) 
				AudioListener.pause = false;
			else
				AudioListener.pause = true;
				
		}
		if(AudioListener.volume == 0f)
			on = false;
		else
			on = true;
		if(on) {
			icon.spriteName = "MenuVolumeButtonVolume3";
			icon.MakePixelPerfect();
			AudioListener.volume = 1f;
			AudioListener.pause = false;
		}
		else {
			icon.spriteName = "MenuVolumeButtonVolume0";
			icon.MakePixelPerfect();
			AudioListener.volume = 0f;
			AudioListener.pause = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClick () {
		on = !on;
		if(on) {
			icon.spriteName = "MenuVolumeButtonVolume3";
			icon.MakePixelPerfect();
			AudioListener.volume = 1f;
			AudioListener.pause = false;
		}
		else {
			icon.spriteName = "MenuVolumeButtonVolume0";
			icon.MakePixelPerfect();
			AudioListener.volume = 0f;
			AudioListener.pause = true;
		}
		PlayerPrefs.SetFloat("volume",AudioListener.volume);
	}
}
