﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {
	public bool RandomizeSoundTrack = true;
	public bool AutoPlayNextSong = true;
	public bool LoopSoundTracks = true;
	public bool PlaySoundTrackOnAwake = true;

    public AudioSource soundTrackSource;
    public AudioSource sfxSource;
    public AudioSource voiceOverSource;

    public AudioClip[] soundTracks;

    public static float SoundTrackVolume {
        get {
            return instance.soundTrackSource.volume;
        }
        set {
            instance.soundTrackSource.volume = value;
        }
    }
    public static float SFXVolume {
        get {
            return instance.sfxSource.volume;
        }
        set {
            instance.sfxSource.volume = value;
        }
    }
    public static float VOVolume {
        get {
            return instance.voiceOverSource.volume;
        }
        set {
            instance.voiceOverSource.volume = value;
        }
    }

    [Range(0.0f,1.0f)]
    public float musicVolume = 1f;
    [Range(0.0f, 1.0f)]
    public float fxVolume = 1f;

    private static SoundController instance;
    void Awake() {
        if(instance != null) {
            Destroy(this.gameObject);
            return;
        }
		if(PlaySoundTrackOnAwake) {
			NextSong();
		}
		soundTrackSource.loop = LoopSoundTracks;
        instance = this;
    }

    // Use this for initialization
	void Start () {
        SoundTrackVolume = musicVolume;
        SFXVolume = fxVolume;
	}

    void Update() {
        if (!soundTrackSource.isPlaying && AutoPlayNextSong) {
            NextSong();
        }
    }

	private int songIndex = 0;

	public static void PlayNextSong() {
		print("Transitioning songs");
		instance.NextSong();
	}

    void NextSong() {
		if(RandomizeSoundTrack) {
        	songIndex = Random.Range(0, soundTracks.Length);
		} else {
			songIndex++;
			if(songIndex >= soundTracks.Length) {
				songIndex = 0;
			}
		}
		StartCoroutine(FadeSoundtrack(soundTracks[songIndex], .5f));
    }

	private bool soundtrackFading = false;
	IEnumerator FadeSoundtrack(AudioClip nextClip, float time) {
		if(soundtrackFading) {
			yield break;
		}
		soundtrackFading = true;
		float t = 0f;
		float startVolume = soundTrackSource.volume;
		float target = time / 2f;
		while(t < time / 2f) { // fade out
			t += Time.deltaTime;
			soundTrackSource.volume = startVolume * (1 - t / target); 
			yield return null;
		}
		soundTrackSource.Stop();
		soundTrackSource.clip = nextClip;
		soundTrackSource.Play ();
		t = 0f;
		while(t < time / 2f) { // fade out
			t += Time.deltaTime;
			soundTrackSource.volume = startVolume * t / target; 
			yield return null;
		}
		soundtrackFading = false;
		soundTrackSource.volume = 1f;
	}

    public static void PlaySFX(AudioClip clip) {
        PlaySFX(clip, 1f);
    }

    public static void PlaySFX(AudioClip clip, float volScale) {
        instance.sfxSource.PlayOneShot(clip, volScale);
    }

    public static void PlayVO(AudioClip vo) {
        PlayVO(vo, 1f);
    }

    public static void PlayVO(AudioClip vo, float volScale) {
        instance.voiceOverSource.PlayOneShot(vo, volScale);
    }
}
