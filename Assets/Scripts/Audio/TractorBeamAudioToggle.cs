﻿using UnityEngine;
using System.Collections;

public class TractorBeamAudioToggle : MonoBehaviour {
	private AudioSource src;
	// Use this for initialization
	void Start () {
		src = this.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if(DetectTractorBeam.InBeam) {
			src.volume = Mathf.Lerp(src.volume,1f,3f * Time.deltaTime);
		} else {
			src.volume = Mathf.Lerp(src.volume,0f,3f * Time.deltaTime);
		}
	}
}
