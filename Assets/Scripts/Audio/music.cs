using UnityEngine;
using System.Collections;

public class music : MonoBehaviour {
	public AudioSource musicSource;
	public AudioClip mainClip;
	public AudioClip altClip;
	private float startVol;
	private static music inst; // This lets us start out of the main menu and what not
	// Use this for initialization
	void Start () {
		if(inst != null) {
			Destroy (this.gameObject);
			return;
		}
		inst = this;
		startVol = musicSource.volume;
		if(PlayerPrefs.HasKey("volume"))
			AudioListener.volume = PlayerPrefs.GetFloat("volume");
		if(AudioListener.volume > 0f) 
			AudioListener.pause = false;
		else
			AudioListener.pause = true;
		musicSource.clip = mainClip;
		musicSource.Play();
		StartCoroutine(SwapRoutine());
		DontDestroyOnLoad(this.gameObject);
	}

	IEnumerator SwapRoutine() {
		while(true) {
			while(Application.loadedLevelName != "highScore") {
				yield return 0;
			}
			yield return StartCoroutine(SwapClips());
			while(Application.loadedLevelName == "highScore") {
				yield return 0;
			}
		}
	}

	IEnumerator SwapClips() {
		AudioClip nextClip;
		if(musicSource.clip == mainClip) {
			nextClip = altClip;
		} else {
			nextClip = mainClip;
		}
		float fadeTime = .8f;
		float fadeElapsed = fadeTime;
		while(fadeElapsed > 0) {
			fadeElapsed -= Time.deltaTime;
			musicSource.volume = startVol * fadeElapsed/fadeTime;
			yield return 0;
		}
		fadeElapsed = 0f;
		musicSource.Stop();
		musicSource.clip = nextClip;
		musicSource.Play();
		while(fadeElapsed < fadeTime) {
			fadeElapsed += Time.deltaTime;
			musicSource.volume = startVol * fadeElapsed/fadeTime;
			yield return 0;
		}

	}
	
	// Update is called once per frame
	void Update () {
		//transform.position = Camera.main.transform.position;

	}
}
