﻿using UnityEngine;
using System.Collections;

public class ActiveCam : MonoBehaviour {
	public Camera bgCam;
	public GameObject bg;
	public float camIncrease;
	private float baseSize;
	// Use this for initialization
	void Start () {
		baseSize = bgCam.orthographicSize;
		StartCoroutine("oscillate");
	}
	
	IEnumerator oscillate() {
		int timer = 0;
		while(true) {
			while(timer < 1000) {
				timer++;
				bgCam.orthographicSize -= .0015f;
				yield return 0;
			}
			timer = 0;
			while(timer < 1000) {
				timer++;
				bgCam.orthographicSize += .0015f;
				yield return 0;
			}
			timer = 0;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Application.loadedLevelName == "survival" || Application.loadedLevelName == "classic") {
			bgCam.orthographicSize = Mathf.Lerp(bgCam.orthographicSize, baseSize + (float)YarnRegister.ball.collected.Count * camIncrease, Time.deltaTime);
		}
		if(Application.loadedLevelName == "loadScene")
			bg.SetActive(false);
		else
			bg.SetActive(true);
	}
}
