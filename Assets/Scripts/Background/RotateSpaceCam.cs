﻿using UnityEngine;
using System.Collections;

public class RotateSpaceCam : MonoBehaviour {
	// Rotates object over time
	public float speed = 5f;
	private float elapsed;
	private float timeToChange = 10f;
	private Vector3 direction;
	// Use this for initialization
	void Start () {
		StartCoroutine("change");
	}
	
	IEnumerator change() {
		ChangeDirection();
		yield return new WaitForSeconds(timeToChange);
		StartCoroutine("change");
	}
	
	void ChangeDirection() {
		direction = new Vector3(Random.Range(0,2),Random.Range(0,2),Random.Range(0,2));
		if(direction.Equals(new Vector3(0,0,0)))
			ChangeDirection();
	}
	
	void RotateMe() {
		transform.Rotate(direction * speed);
	}
	
	// Update is called once per frame
	void Update () {
		RotateMe();
	}
}
