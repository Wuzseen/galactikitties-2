﻿ using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TexGen : MonoBehaviour {
	public tk2dTileMap map;
	private int x = 0, y = 0, c = 0;
	private int tileCount;
	public int firstPlanetIndex, planetFrequency, variance;
	private int baseFreq;
	private List<int> planets, usedPlanets;
	
	// Use this for initialization
	void Start () {
		planets = new List<int>();
		tileCount = map.data.tilePrefabs.Length;
		baseFreq = planetFrequency;
		usedPlanets = new List<int>();
		for(int i = firstPlanetIndex; i < tileCount; i++) {
			planets.Add(i);
			usedPlanets.Add(i);
		}
		Generate();
	}
	
	// Update is called once per frame
	void Generate () {
		x += 64;
		y += 64;
		for(int i = 0; i < x; i++) {
			for(int z = 0; z < y; z++) {
				if(map.GetTile(i,z,0) < 0) {
					int thetile = Random.Range(0,firstPlanetIndex);
					if(c % planetFrequency != 0)
						map.SetTile(i,z,0,thetile);
					else  {
						map.SetTile(i,z,0,pickPlanet());
						NewVariance();
					}
					c++;
				}
			}
		}
		map.Build();
	}
	
	int pickPlanet() {
		if(usedPlanets.Count == 0) {
			foreach(int z in planets)
				usedPlanets.Add(z);
		}
		int rIndex = Random.Range(0,usedPlanets.Count);
		int rVal = usedPlanets[rIndex];
		usedPlanets.Remove(rIndex);
		return rVal;
	}
	
	void NewVariance() {
		int posNeg = Random.Range(0,2) == 1 ? 1 : -1;
		planetFrequency = baseFreq + Random.Range(0,variance) * posNeg;
		c = 1;
	}
	
	void Update () {
	}
}
