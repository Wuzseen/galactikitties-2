﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {
	public bool plus; // True means it selects the next, false means the previous
	public ObjectSelector objSelect;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClick () {
		if(plus)
			objSelect.change(Changes.Increment);
		else
			objSelect.change(Changes.Decremement);
	}
}
