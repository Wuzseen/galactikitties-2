﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.IO;

public class CatData {
	[XmlElement]
	public string skinName;
	[XmlElement]
	public string flairName;
	[XmlElement]
	public string catName;
	[XmlElement]
	public string colorIndex;
	[XmlElement]
	public string g;
	[XmlElement]
	public string b;
	public int index;
	
	public CatData() {
		catName = "Kitty";	
		skinName = "none";
		flairName = "none";
	}
	
	public CatData(string name,int index_) {
		catName = name;
		skinName = "none";
		flairName = "none";
		colorIndex = "0";
		index = index_;
	}
	
	public void Save() {
		PlayerPrefs.SetString(index.ToString() + "_name",catName);
		PlayerPrefs.SetString(index.ToString() + "_colorIndex",colorIndex);
		PlayerPrefs.SetString(index.ToString() + "_skin",skinName);
		PlayerPrefs.SetString(index.ToString() + "_flair",flairName);
	}
	
	public static CatData DefaultCat(int catNo) {
		CatData cd = new CatData();
		switch(catNo) {
		case 1:
			cd.catName = "Rupert";
			cd.index = 1;
			cd.colorIndex = "5";
			break;
		case 2:
			cd.catName = "Pepper";
			cd.index = 0;
			cd.colorIndex = "6";
			break;
		case 3:
			cd.catName = "Sebastian";
			cd.index = 2;
			cd.colorIndex = "2";
			break;
		}	
		cd.skinName = "default";
		return cd;
	}
	
	public override string ToString() {
		return "Name: " + catName + " Skin: " + skinName + " Color: " + colorIndex;
	}
	
	public static CatData CatDataFromPrefs(int index) { // cat1, cat2, cat3
		CatData cd = new CatData();
		cd.catName = PlayerPrefs.GetString(index.ToString() + "_name");
		cd.colorIndex = PlayerPrefs.GetString(index.ToString() + "_colorIndex");
		cd.skinName = PlayerPrefs.GetString(index.ToString() + "_skin");
		cd.flairName = PlayerPrefs.GetString(index.ToString() + "_flair");
		cd.index = index;
		return cd;
	}
	
	public Color GetColor() {
		return CatDataMono.catColors[int.Parse(colorIndex)];	
	}
	
	public CatData(string name, string fName, string sName, int cIndex) {
		catName = name;
		skinName = sName;
		flairName = fName;
		colorIndex = cIndex.ToString();
	}
}
