﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

[XmlRoot]
public class CatDataMaster {
	[XmlElement]
	public CatData cat1;
	[XmlElement]
	public CatData cat2;
	[XmlElement]
	public CatData cat3;
	// Use this for initialization
	
	
	public CatDataMaster() {
		cat1 = new CatData("Sebastian",0);
		cat2 = new CatData("Rupert",1);
		cat3 = new CatData("Pepper",2);
	}
	
	public static void Save(CatDataMaster data) {
		data.cat1.Save();
		data.cat2.Save();
		data.cat3.Save();
	}
	
	public static CatDataMaster CreateNewCats() {
		CatDataMaster cdm = new CatDataMaster();
		cdm.cat1 = CatData.DefaultCat(1);
		cdm.cat2 = CatData.DefaultCat(2);
		cdm.cat3 = CatData.DefaultCat(3);
		return cdm;
	}
	
	public void Print() {
		Debug.Log (cat1.ToString());
		Debug.Log (cat2.ToString());
		Debug.Log (cat3.ToString());
	}
	
	public static CatDataMaster LoadCats() {
		CatDataMaster cdm = new CatDataMaster();
		cdm.cat1 = CatData.CatDataFromPrefs(0);
		cdm.cat2 = CatData.CatDataFromPrefs(1);
		cdm.cat3 = CatData.CatDataFromPrefs(2);
		return cdm;
	}
}
