﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CatDataMono : MonoBehaviour {
	public static CatDataMaster data;
	public static Color[] catColors = new Color[7];
	public static Dictionary<string,string> textureToName = new Dictionary<string, string>
	{{"topHat","Top Hat"},
		{"none","None"}};
	// Use this for initialization
	void Awake () {
		//Screen.showCursor = false;
		if(data != null) {
			Destroy (this.gameObject);
			return;
		}
		CatDataMaster newData;
		//PlayerPrefs.DeleteAll();
		if(PlayerPrefs.HasKey("catsLoaded"))
			newData = CatDataMaster.LoadCats();
		else {
			newData = CatDataMaster.CreateNewCats();
			PlayerPrefs.SetString("catsLoaded","true");
		}
		data = new CatDataMaster();
		SetColors();
		data.cat1 = newData.cat1;
		data.cat2 = newData.cat2;
		data.cat3 = newData.cat3;
		Save ();
		DontDestroyOnLoad(this);
	}

	private void SetColors() {
		catColors[0] = new Color(97f/255f, 65f/255f, 102f/255f); // Red
		catColors[1] = new Color(199f/255f,131f/255f,58f/255f); // Pink
		catColors[2] = new Color(110f/255f,66f/255f,44f/255f); // Brown
		catColors[3] = new Color(138f/255f,0f/255f,199f/255f);
		catColors[4] = new Color(90f/255f,102f/255f,79f/255f);
		catColors[5] = new Color(132f/255f,176f/255f,175f/255f); // Gray
		catColors[6] = new Color(250f/255f,226f/255f,197f/255f); // Yellow
	}
	
	public static void ReLoad() {
		data = CatDataMaster.LoadCats();
	}
	
	public static void Save() {
		CatDataMaster.Save(data);	
	}
	
	// Update is called once per frame
	void Update () {
//		print (data == null);
	}
}
