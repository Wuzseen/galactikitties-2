﻿using UnityEngine;
using System.Collections;

public class ChangeCatArrow : MonoBehaviour {
	public CustomCat ccat;
	public bool increase;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClick () {
		if(increase)
			ccat.nextCat();
		else
			ccat.previousCat();
	}
}
