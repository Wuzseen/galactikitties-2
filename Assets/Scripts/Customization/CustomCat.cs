﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Spine;

public class CustomCat : MonoBehaviour {
	public SkeletonAnimation skele;
	public UILabel catNameLabel;
	//private static string[] coloredParts = {"Back-Leg-2","Front-Leg-2","Back-Leg-1","Front-Leg-1","Tail","Body","Right-Ear","Left-Ear","Head"};
	private int selected;
	public Skinner lst;
	private CatData selectedCat;
	
	// Use this for initialization
	void Start () {
		StartCoroutine("Animator");
	}
	
	IEnumerator WaitForLoad() {
		while(skele == null) {	
			yield return 0;
		}
		loadSelectedCat();
	}
	
	public void LoadCustomKitty() { // Waits for cat dictionaries to be loaded
		if(PlayerPrefs.HasKey("selectScreenCat")) {
			selected = PlayerPrefs.GetInt("selectScreenCat");
		} else {
			selected = 0;
		}
		StartCoroutine("WaitForLoad");
	}
	
	IEnumerator Animator() {
		while(true) {
			int animation = Random.Range(0,3);
			if(animation == 0) {
				skele.state.SetAnimation(0,"earWaggle",true);
			} else if(animation == 1) {
				skele.state.SetAnimation(0,"tailWaggle",true);
			} else if(animation == 2) {
				skele.state.SetAnimation(0,"feetWaggle",true);
			}
			yield return new WaitForSeconds(5f);
		}
	}
	
	public void setSkin(string skinName) {
		if(skele.skeleton == null)
			return;
		string c = "";
		if(selected == 0) {
			CatDataMono.data.cat1.skinName = skinName;
			c = CatDataMono.data.cat1.colorIndex;
		} else if(selected == 1) {
			CatDataMono.data.cat2.skinName = skinName;
			c = CatDataMono.data.cat2.colorIndex;
		} else if(selected == 2) {
			CatDataMono.data.cat3.skinName = skinName;
			c = CatDataMono.data.cat3.colorIndex;
		}
		skele.skeleton.SetSkin(skinName);
		skele.skeleton.SetSlotsToSetupPose();
		Dresser.SetSkeleColor(skele,int.Parse(c));
		CatDataMono.Save();
	}

	public void SetColor(int c) {
		Dresser.SetSkeleColor(skele, c);
		
		if(selected == 0) {
			CatDataMono.data.cat1.colorIndex = (c).ToString();
		} else if(selected == 1) {
			CatDataMono.data.cat2.colorIndex = (c).ToString();
		} else if (selected == 2) {
			CatDataMono.data.cat3.colorIndex = (c).ToString();
		}
		CatDataMono.Save();
	}
	
	public void nextCat() {
		selected++;
		if(selected > 2)
			selected = 0;
		loadSelectedCat();
	}
	
	public void previousCat() {
		selected--;
		if(selected < 0)
			selected = 2;
		loadSelectedCat();
	}
	
	public void loadSelectedCat() {
		PlayerPrefs.SetInt("selectScreenCat",selected);
		if(selected == 0) {
			Dresser.SetSkeleColor(skele,int.Parse(CatDataMono.data.cat1.colorIndex));
			setSkin(CatDataMono.data.cat1.skinName);
			catNameLabel.text = CatDataMono.data.cat1.catName;
		} else if(selected == 1) {
			Dresser.SetSkeleColor(skele,int.Parse(CatDataMono.data.cat2.colorIndex));
			setSkin(CatDataMono.data.cat2.skinName);
			catNameLabel.text = CatDataMono.data.cat2.catName;
		} else if (selected == 2) {
			Dresser.SetSkeleColor(skele,int.Parse(CatDataMono.data.cat3.colorIndex));
			setSkin(CatDataMono.data.cat3.skinName);
			catNameLabel.text = CatDataMono.data.cat3.catName;
		}
		lst.LoadKitty(selected);
	}
	
	void OnSubmit() {
		if(selected == 0)
			CatDataMono.data.cat1.catName = catNameLabel.text;
		else if(selected == 1)
			CatDataMono.data.cat2.catName = catNameLabel.text;
		else if(selected == 2)
			CatDataMono.data.cat3.catName = catNameLabel.text;
			
		CatDataMono.Save();
	}
}
