﻿using UnityEngine;
using System.Collections;

public class CustomizerTextButtonLabel : MonoBehaviour {
	public tk2dSprite theSprite;
	public UILabel lbl;
	public bool isOn = true;
	// Use this for initialization
	void Start () {
		lbl.text = theSprite.CurrentSprite.name;
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	public void ChangeText(string text) {
		if(text != null)
			lbl.text = text;
		else
			lbl.text = "Disabled";
	}
}
