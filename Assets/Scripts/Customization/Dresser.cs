﻿using UnityEngine;
using System.Collections;
using Spine;

public class Dresser : MonoBehaviour {
	
	public static void SetSkeleColor(SkeletonAnimation skele, int cIndex) {
		//if(skele.skeleton == null)
			//return;
		Color c = CatDataMono.catColors[cIndex];
		foreach(Slot part in skele.skeleton.Slots) {
			if(part.Attachment != null) {
				if(!part.Attachment.Name.Contains("item") && !part.Attachment.Name.Contains("wing") && !part.Attachment.Name.Contains("Face")) {
					part.r = c.r;
					part.g = c.g;
					part.b = c.b;
				}
			}
		}
	}
}
