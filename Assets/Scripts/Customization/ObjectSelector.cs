using UnityEngine;
using System.Collections;

public enum Changes { Toggle, Increment, Decremement, DoNothing }

// This class makes an arrow based selector of gameobjects.  EG, select a bunch of things.
public class ObjectSelector : MonoBehaviour {
	public bool wearing;
	public tk2dSprite theSprite;
	public CustomizerTextButtonLabel labeler;
	private int index;
	// Use this for initialization
	void Start () {
		change (Changes.DoNothing);
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	public void change(Changes cVal) {
		if(cVal == Changes.Increment)
			index = index == theSprite.Collection.Count - 1 ? 0 : index + 1;
		else if(cVal == Changes.Decremement)
			index = index == 0 ? theSprite.Collection.Count - 1 : index - 1;
		
		if(wearing) {
			theSprite.enabled = true;
			theSprite.GetComponent<Renderer>().enabled = true;
			theSprite.GetComponent<Collider>().enabled = true;
			theSprite.SetSprite(index);
			labeler.ChangeText(CatDataMono.textureToName[theSprite.CurrentSprite.name]);
		} else {
			labeler.ChangeText(null);
			theSprite.enabled = false;
			theSprite.GetComponent<Renderer>().enabled = false;
			theSprite.GetComponent<Collider>().enabled = false;
		}
	}	
}