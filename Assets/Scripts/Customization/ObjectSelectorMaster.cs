﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectSelectorMaster : MonoBehaviour {
	public GameObject monoPrefab;
	public int catInt;
	public string catName;
	public ObjectSelector skin;
	public ObjectSelector flair;
	public int catIndex;
	private bool saved = false;
	// Use this for initialization
	void Start () {
		if(CatDataMono.data == null) {
			Instantiate(monoPrefab);
		}
	}
	
	public void NameUpdate() {
		switch(catInt) {
		case 1:
			catName = CatDataMono.data.cat1.catName;
			break;
		case 2:
			catName = CatDataMono.data.cat2.catName;
			break;
		case 3:
			catName = CatDataMono.data.cat3.catName;
			break;
		}		
	}
	
	// Update is called once per frame
	void Update () {
		if(Time.timeSinceLevelLoad > 2f && !saved) {
			saved = true;	
			SaveAllCats();
		}
	}
	
	public void SaveAllCats() {
		switch(catInt) {
		case 1:
			CatDataMono.data.cat1 = new CatData(catName,flair.theSprite.CurrentSprite.name,skin.theSprite.CurrentSprite.name,catIndex);
			break;
		case 2:
			CatDataMono.data.cat2 = new CatData(catName,flair.theSprite.CurrentSprite.name,skin.theSprite.CurrentSprite.name,catIndex);
			break;
		case 3:
			CatDataMono.data.cat3 = new CatData(catName,flair.theSprite.CurrentSprite.name,skin.theSprite.CurrentSprite.name,catIndex);
			break;
		}
		CatDataMaster.Save(CatDataMono.data);
	}
	
	void OnDestroy () {
		SaveAllCats ();
	}
}
