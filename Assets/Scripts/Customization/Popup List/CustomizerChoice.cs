﻿using UnityEngine;
using System.Collections;

public class CustomizerChoice : MonoBehaviour {
	public Color selectedColor;
	public CustomizerPopupList popupList;
	public UISprite spriteToColor;
	public UISprite fill;
	public UILabel lbl;
	// Use this for initialization
	void Start () {
		fill.fillAmount = 1f;
	}

	void OnClick () {
		popupList.ActivateChoice(this);
		Colorize();
	}

	public void Colorize() {
		spriteToColor.color = selectedColor;
	}
}
