﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomizerPopupList : MonoBehaviour {
	private Dictionary<string,CustomizerChoice> items;
	public GameObject choicePre;
	public Color defaultColor;
	public Transform startPos;
	public float offset;
	public float boxScale = 1.2f;
	public CustomizerChoice normal;
	public Skinner skinner;
	private CustomizerChoice activeChoice;
	public string Value {
		get{ return activeChoice.lbl.text; }
	}
	private int i;
	// Use this for initialization
	public void Initialize () {
		items = new Dictionary<string, CustomizerChoice>();
		items.Add("Normal",normal);
		ActivateChoice("Normal");
		i = 1;
	}

	public void Add(string aChoice) {
		Vector3 pos = new Vector3(startPos.localPosition.x, startPos.localPosition.y - offset * i, 0);
		GameObject o = (GameObject)Instantiate(choicePre);
		o.transform.parent = this.transform;
		o.transform.localPosition = pos;
		o.transform.localScale = new Vector3(boxScale,boxScale,boxScale);
		CustomizerChoice choice = o.GetComponent<CustomizerChoice>();
		choice.popupList = this;
		choice.lbl.text = aChoice;
		if(items == null)
			items = new Dictionary<string, CustomizerChoice>();
		items.Add(aChoice,choice);
		i++;
	}

	public void ActivateChoice(string aChoice) {
		if(activeChoice != null)
			activeChoice.spriteToColor.color = defaultColor;
		CustomizerChoice choice = null;
		if(items.ContainsKey(aChoice)) {
			choice = items[aChoice];
		} else { //  Only possible after a preference clear, janky--but would require a big rewrite probably to do correctly.  This should do the trick.
			ActivateChoice("Normal");
		}
		if(choice == null) 
			return;
		activeChoice = choice;
		activeChoice.Colorize();
		skinner.SelectionChange(activeChoice.lbl.text);
	}

	public void ActivateChoice(CustomizerChoice aChoice) {
		activeChoice.spriteToColor.color = defaultColor;
		activeChoice = aChoice;
		skinner.SelectionChange(activeChoice.lbl.text);
	}
}
