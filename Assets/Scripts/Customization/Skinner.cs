﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skinner : MonoBehaviour {
	private Dictionary<string,string> visibleSkinToSpine = new Dictionary<string, string>();
	private Dictionary<string,string> reversed = new Dictionary<string, string>();
	private Dictionary<string,int> costDict = new Dictionary<string, int>();
	private List<string> unlockedCostumes;
	public CustomCat kitty;
	public UILabel lbl;
	public CustomizerPopupList lst;

	void Awake() {
		Achievements.Instance.PropagatePlatinum();
		Dictionary<string,AchievementInfo> achievements = Achievements.Instance.GetAllAchievementInfo();
		lst.Initialize();
		if(GoogleAnalytics.instance) {
			GoogleAnalytics.instance.LogScreen("Customizing");
		}
		if(achievements["Cat Collector"].IsUnlocked())
			lst.Add("Fancy");
		if(achievements["Time Cat"].IsUnlocked())
			lst.Add("Cool Cat");
		if(achievements["Feline' Fine"].IsUnlocked())
			lst.Add("Angelic");
		if(achievements["Catnip Dealer"].IsUnlocked())
			lst.Add("Wizard");
		if(achievements["Fish Fiend"].IsUnlocked())
			lst.Add("Santa");
		if(achievements["Nine Lives"].IsUnlocked())
			lst.Add("Fiesta");
		if(achievements["Neeeenja"].IsUnlocked())
			lst.Add("Ninja");
		if(achievements["Smart Cat"].IsUnlocked())
			lst.Add("Astronaut");
		if(achievements["Silly Cat"].IsUnlocked())
			lst.Add("Nerd");
		if(achievements["The Purrfesional"].IsUnlocked())
			lst.Add("Afro");
		if(achievements["Catamari"].IsUnlocked())
			lst.Add("Robot");
		if(achievements["Kitty Hero"].IsUnlocked())
			lst.Add("Doctor");
		if(achievements["Platinum"].IsUnlocked())
			lst.Add("Victory");
		if(achievements["Dance the Space Hula"].IsUnlocked())
			lst.Add("Hula");
		if(achievements["Like a Vet"].IsUnlocked())
			lst.Add("Dragon");
		CatDictHelper("Normal","default",100);
		CatDictHelper("Fancy","fancy",100);
		CatDictHelper("Cool Cat","fez",100);
		CatDictHelper("Angelic","angel",100);
		CatDictHelper("Wizard","magic",100);
		CatDictHelper("Santa","santa",100);
		CatDictHelper("Fiesta","fiesta",100);
		CatDictHelper("Ninja","ninja",100);
		CatDictHelper("Astronaut","astronaut",100);
		CatDictHelper("Nerd","nerd",100);
		CatDictHelper("Afro","afro",100); 
		CatDictHelper("Robot","robot",100);
		CatDictHelper("Doctor","doctor",100);
		CatDictHelper("Victory","trophy",100);
		CatDictHelper("Hula","hula",100);
		CatDictHelper("Dragon","dragon",100);
	}

	// Use this for initialization
	void Start () { // This might need to be awake.  Not sure.  Fixes a problem, but I remember this being a problem in the past if it wasn't awake.  Maybe that's been fixed since.
		// This is horible.  And should change... butttttttttttt

		unlockedCostumes = Skinner.UnlockedCostumes();	
		kitty.LoadCustomKitty();
		
	}

	// Adds a cat to the skinner
	private void CatDictHelper(string visible, string spine, int cost) {
		visibleSkinToSpine.Add(visible,spine);
		costDict.Add(spine,cost);
		reversed.Add(spine,visible);
	}

	public List<string> getUnlockedCostumes() {
		return unlockedCostumes;	
	}
	
	public string getSpineName(string costumeName) {
		return visibleSkinToSpine[costumeName];
	}
	
	public int getCost(string costumeName) { // Uses spine name
		return costDict[costumeName];
	}
	
	public static List<string> UnlockedCostumes() {
		if(PlayerPrefs.HasKey("unlockedCostumes_")) {
			return new List<string>(PlayerPrefs.GetString("unlockedCostumes_").Split(','));
		} else {
			List<string> unlocks = new List<string>();
			unlocks.Add("default");
			PlayerPrefs.SetString("unlockedCostumes_","default");
			return unlocks;
		}
	}
	
	public static void UnlockCostume(string costumeName) {
		if(PlayerPrefs.HasKey("unlockedCostumes_")) {
			PlayerPrefs.SetString("unlockedCostumes_",PlayerPrefs.GetString("unlockedCostumes_") + "," + costumeName);
			return;
		} else {
			PlayerPrefs.SetString("unlockedCostumes_","default," + costumeName);	
			return;
		}
	}
	
	public static bool IsUnlocked(string costumeName) {
		foreach(string s in UnlockedCostumes()) {
			if( s.Equals(costumeName) )
				return true;
		}
		return false;
	}
	
	public void LoadKitty(int selected) {
		if(selected == 0) {
			lst.ActivateChoice(reversed[CatDataMono.data.cat1.skinName]);	
		}
		if(selected == 1) {
			lst.ActivateChoice(reversed[CatDataMono.data.cat2.skinName]);
		}
		
		if(selected == 2) {
			lst.ActivateChoice(reversed[CatDataMono.data.cat3.skinName]);
		}
	}
	/*
	public void SelectionChange() {
		try {
			kitty.setSkin(visibleSkinToSpine[lst.value]);
		} catch {	
			print ("Skin DNE");
		}
	}
	*/
	public void SelectionChange(string aValue) {
		try {
			kitty.setSkin(visibleSkinToSpine[aValue]);
			lbl.text = aValue;
		} catch {	

		}
	}
}
