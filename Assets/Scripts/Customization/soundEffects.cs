using UnityEngine;
using System.Collections;

public class soundEffects : MonoBehaviour {

	public AudioClip[] meows;
	public AudioClip[] asteroid;
	public AudioClip[] magnet;
	public AudioClip[] sticks;
	public AudioClip[] collectNoises;
	public AudioClip newLevelSound;
	public AudioClip tractNoise;
	public AudioClip laserShot;
	private static soundEffects instance;
	public static soundEffects Instance {
		get { return instance; }
	}

	private static AudioClip statLevel;
	private static AudioClip[] statMeows;
	private static AudioClip[] statAsteroids;
	private static AudioClip[] statMagnet;
	private static AudioClip[] statCollects;
	private static AudioClip[] statStick;
	private static AudioClip statLaser;
	private static bool waitOnCollectNoise;
	// Use this for initialization
	void Start () {
		instance = this;
		statCollects = collectNoises;
		statMeows = meows;
		statAsteroids = asteroid;
		statStick = sticks;
		statLevel = newLevelSound;
		statMagnet = magnet;
		//statTract = tractNoise;
	}
	
	public static void newLevel() {
		if(!AudioListener.pause) {
			SoundController.PlaySFX(statLevel);
		}
	}
	
	public static void randomMeow () {
		if(!AudioListener.pause) {
			SoundController.PlaySFX(statMeows[Random.Range(0,statMeows.Length)],4f);
		}
	}
	
	public static void randomAsteroid () {
		if(!AudioListener.pause) {
			SoundController.PlaySFX(statAsteroids[Random.Range(0,statAsteroids.Length)],.5f);
		}
	}

	public static void randomMagnet () {
		if(!AudioListener.pause) {
			SoundController.PlaySFX(statMagnet[Random.Range(0,statMagnet.Length)]);
		}
	}
	
	public static void randomStick () {
		if(!AudioListener.pause) {
			SoundController.PlaySFX(statStick[Random.Range(0,statStick.Length)]);
		}
	}

	public void randomCollect () {
		if (!AudioListener.pause && !waitOnCollectNoise) {
			waitOnCollectNoise = true;
			SoundController.PlaySFX(collectNoises [Random.Range (0, collectNoises.Length)]);
			StartCoroutine (collectWait());
		}
	}

	private IEnumerator collectWait () {
		yield return new WaitForSeconds (.4f);
		waitOnCollectNoise = false;
	}

	public void PlayLaserShot () {
		if(!AudioListener.pause) {
			SoundController.PlaySFX(laserShot);
		}
	}
	
	public static void tractorBeam () {
		//src.PlayOneShot(statTract);	
	}
}
