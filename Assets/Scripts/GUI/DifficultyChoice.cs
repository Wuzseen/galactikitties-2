﻿using UnityEngine;
using System.Collections;

public class DifficultyChoice : MonoBehaviour {
	public DifficultyChooser chooser;
	public string diffString;
	// Use this for initialization
	void Start () {
	
	}

	void OnPress(bool isDown) {
		if(isDown) {
			chooser.SetDifficulty(diffString);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
