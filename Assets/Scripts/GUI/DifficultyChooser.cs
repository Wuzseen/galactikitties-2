﻿using UnityEngine;
using System.Collections;

public class DifficultyChooser : MonoBehaviour {
	public UISprite spr;

	void Start() {
		SetDifficulty(Difficulty.ActiveDifficulty.ToString());
	}

	public void SetDifficulty(string difficulty) {
		Difficulty.SetDifficulty(difficulty);
		if(Difficulty.ActiveDifficulty == DifficultyLevel.Easy) {
			spr.spriteName = "easyDifficulty";
		} else if(Difficulty.ActiveDifficulty == DifficultyLevel.Medium) {
			spr.spriteName = "mediumDifficulty";
		} else {
			spr.spriteName = "hardDifficulty";
		}
	}
}
