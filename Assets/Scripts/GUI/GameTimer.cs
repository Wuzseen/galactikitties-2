﻿using UnityEngine;
using System.Collections;

public class GameTimer : MonoBehaviour {
	public UISprite sprite;
	// Use this for initialization
	void Start () {
		StartCoroutine("forward",5f);
	}

	IEnumerator forward(float time) {
		float timer = 0;
		while(timer < time) {
			sprite.fillAmount = timer / time;
			timer += Time.deltaTime;
			yield return 0;
		}
		StartCoroutine("backward",5f);
	}

	IEnumerator backward(float time) {
		float timer = time;
		while(timer >= 0) {
			sprite.fillAmount = timer / time;
			timer -= Time.deltaTime;
			yield return 0;
		}
		StartCoroutine("forward",5f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
