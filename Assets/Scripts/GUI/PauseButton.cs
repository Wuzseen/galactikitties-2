﻿using UnityEngine;
using System.Collections;

public class PauseButton : MonoBehaviour {
	public UISprite sprite;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnPress (bool isDown) {
		if(isDown) {
			sprite.spriteName = sprite.spriteName == "playButton" ? "pauseButton" : "playButton";
			YarnRegister.ball.paused = !YarnRegister.ball.paused;
		}
	}
}
