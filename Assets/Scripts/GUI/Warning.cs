﻿using UnityEngine;
using System.Collections;

public class Warning : MonoBehaviour {
	public UILabel lbl;
	private static Warning instance;
	// Use this for initialization
	void Awake () {
		lbl = this.GetComponent(typeof(UILabel)) as UILabel;
		lbl.alpha = 0f;
		instance = this;
	}

	// .2 second ramp in and out
	public static void Alert(string message, float timeUp) {
		if(instance == null) {
			return;
		}
		instance.lbl.text = message;
		instance.StartCoroutine("ShowAlert",timeUp);
	}

	IEnumerator ShowAlert(float time) {
		Go.to(lbl, .2f,new GoTweenConfig().floatProp("alpha",1f,false));
		yield return new WaitForSeconds(.2f + time);
		Go.to(lbl, .2f,new GoTweenConfig().floatProp("alpha",0f,false));
	}

	public static void Alert(string message) {
		Alert (message,2f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
