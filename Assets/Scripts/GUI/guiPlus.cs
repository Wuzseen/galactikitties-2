using UnityEngine;
using System.Collections;

public class guiPlus : MonoBehaviour {
	public GUIStyle style;
	private float timer, timerLimit;
	private string txtVal;
	private bool display = false;
	
	public void activate(float timeLimit, string text) {
		display = true;
		txtVal = text;
		Destroy (this.gameObject,timeLimit);
		timer = 0;
	}
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(display)
			timer += Time.deltaTime * 4;
	}
	
	void OnGUI() {
		if(display)
			GUI.Label(new Rect(Screen.width/2 - 100, 100 + timer,200,200), "+" + txtVal, style);	
	}
}
