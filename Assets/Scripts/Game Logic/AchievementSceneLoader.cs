﻿using UnityEngine;
using System.Collections;

public class AchievementSceneLoader : MonoBehaviour {
	
	public UILabel classicCats, classicScore, timeScore, 
			timeCats, survivalTime;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		classicCats.text =   PlayerPrefs.GetInt("ClassicCats") + " / " + Achievements.Instance.getAchieveRequirement("ClassicCats");
		classicScore.text =    PlayerPrefs.GetInt("ClassicScore") + " / " + Achievements.Instance.getAchieveRequirement("ClassicScore");
		timeCats.text =    PlayerPrefs.GetInt("TimeCats") + " / " + Achievements.Instance.getAchieveRequirement("TimeCats");
		timeScore.text =  PlayerPrefs.GetInt("TimeScore") + " / " + Achievements.Instance.getAchieveRequirement("TimeScore");
		survivalTime.text = PlayerPrefs.GetInt ("SurvivalTime") + " / " + Achievements.Instance.getAchieveRequirement("SurvivalTime");
	}
}