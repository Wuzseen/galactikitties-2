﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Achievements : MonoBehaviour {
	int[] classicCatsRequired = new int[3];
	int[] timeAttackCatsRequired = new int[3];
	int[] survivalTimeRequired = new int[3];
	int[] classicScoreRequired = new int[3];
	int[] timeAttackScoreRequired = new int[3];
	public int highestClassicCats, highestTimeCats, highestSurvivalTime, 
		highestClassicScore, highestTimeScore;
	public string AchievementSplitString = "ACHVSPLT";
	private Dictionary<string, AchievementInfo> achievements;
	public static Achievements Instance;

	// Use this for initialization
	private void AddAchievement(string name, string info, int progress, int required) {
		AchievementInfo newAchievement = new AchievementInfo();
		newAchievement.Title = name;
		newAchievement.Info = info;
		newAchievement.Progress = progress;
		newAchievement.Total = required;

		achievements.Add(name,newAchievement);
	}

	public void SaveAchievements() {
		List<string> nameStrings = new List<string>();
		List<string> infoStrings = new List<string>();
		List<string> progStrings = new List<string>();
		List<string> totalStrings = new List<string>();
		foreach(string key in achievements.Keys) {
			AchievementInfo achievement = achievements[key];
			nameStrings.Add(achievement.Title);
			infoStrings.Add(achievement.Info);
			progStrings.Add(achievement.Progress.ToString());
			totalStrings.Add(achievement.Total.ToString());
		}

		SaveList("AchievementNames",nameStrings);
		SaveList("AchievementInfo",infoStrings);
		SaveList("AchievementProgress",progStrings);
		SaveList("AchievementTotals",totalStrings);
	}

	private void SaveList(string keyToSave, List<string> listToSave) {
		string bigString = listToSave[0];
		for(int i = 1; i < listToSave.Count; i++) {
			bigString += string.Format("{0}{1}",'\t',listToSave[i]);
		}
		PlayerPrefs.SetString(keyToSave,bigString);
	}

	public void UnlockAll() {
		foreach(string key in achievements.Keys) {
			achievements[key].Progress = achievements[key].Total + 1;
		}
	}

	public void ClearPrefsAndAchievements() {
		PlayerPrefs.DeleteAll();
		achievements = new Dictionary<string, AchievementInfo>();
		CreateDefaultAchievements();
	}

	private void CreateDefaultAchievements() {
		print ("Creating defaults.");
		AddAchievement(
			"Cat Collector",
			"Save cats in Normal mode.\nGet them into the portal!\nUnlocks the Fancy cat.",
			0,
			1000
			);
		AddAchievement(
			"Time Cat",
			"Save cats in Time Attack mode.\nCollect cats to rescue them!\nUnlocks the Cool Cat!",
			0,
			1000
			);
		AddAchievement(
			"Feline' Fine",
			"Save cats in Survival mode.\nUnlocks the Angel skin.",
			0,
			1000
			);
		AddAchievement(
			"Catnip Dealer",
			"Cats love them that catnip.\nUnlocks the Wizard skin.",
			0,
			100
			);
		AddAchievement(
			"Fish Fiend",
			"Feed fish to your cats!\nUnlocks the Santa skin.",
			0,
			100
			);
		AddAchievement(
			"Nine Lives",
			"Last a minute in\nSurvival mode.\n9 times.\nUnlocks Fiesta skin.",
			0,
			9
			);
		AddAchievement(
			"Neeeenja",
			"Last a minute in any mode without being hit by anything.  Not a cat, not an asteroid... nothing!\nUnlocks the Ninja skin.",
			0,
			1
			);
		AddAchievement(
			"Smart Cat",
			"Do the simple thing.\nBeat the tutorial!\nUnlocks the Astronaut skin.",
			0,
			1
			);
		AddAchievement(
			"Silly Cat",
			"Finish the Tutorial twice.\n...Why?\nUnlocks the Nerd skin.",
			0,
			2
			);
		AddAchievement(
			"The Purrfesional",
			"Reach Level 5 in any mode.\nUnlocks the Afro skin.",
			0,
			1
			);
		AddAchievement(
			"Catamari",
			"Save cats in any mode.\nUnlocks the Robot skin.",
			0,
			10000
			);
		AddAchievement(
			"Platinum",
			"Unlock 11 achievements. Me-WOW!!\nUnlocks the Victory cat.",
			0,
			11
			);
		FirstUpdateAchievementAdditions();
		SaveAchievements();
	}

	// Horrible hack, but it's low impact.  Make a new function here and conditional in load for every update of achievements
	void FirstUpdateAchievementAdditions() {
		PlayerPrefs.SetInt("1_1Achievements",1);
		AddAchievement(
			"Like a Vet",
			"Collect 10 health crates in Survival.\nUnlocks the Dragon cat.",
			0,
			10
			);
		AddAchievement(
			"Kitty Hero",
			"Tap the ASPCA logo on the home screen\nUnlocks the Doctor cat.",
			0,
			1
			);
		AddAchievement(
			"Dance the Space Hula",
			"Achievement unlocked... for being awesome.\nUnlocks the Hula cat.",
			1,
			1
			);
	}

	public void LoadAchievements() {
		if(!PlayerPrefs.HasKey("AchievementNames")) {
			CreateDefaultAchievements();
		} else {
			List<string> nameStrings = new List<string>(PlayerPrefs.GetString("AchievementNames").Split('\t'));
			List<string> infoStrings = new List<string>(PlayerPrefs.GetString("AchievementInfo").Split('\t'));
			List<string> progStrings = new List<string>(PlayerPrefs.GetString("AchievementProgress").Split('\t'));
			List<string> totalStrings = new List<string>(PlayerPrefs.GetString("AchievementTotals").Split('\t'));
			if(achievements == null)
				achievements = new Dictionary<string,AchievementInfo>();
			for(int i = 0; i < nameStrings.Count; i++) {
				AchievementInfo achInfo = new AchievementInfo();
				achInfo.Title = nameStrings[i];
				achInfo.Progress = int.Parse(progStrings[i]);
				achInfo.Total = int.Parse(totalStrings[i]);
				achInfo.Info = infoStrings[i];
				achievements.Add(achInfo.Title,achInfo);
			}
			if(!PlayerPrefs.HasKey("1_1Achievements")) {
				FirstUpdateAchievementAdditions();
			}
		}
	}

	public void PropagatePlatinum()
	{
		int i = 0;
		foreach(string key in achievements.Keys) {
			if(achievements[key].IsUnlocked())
				i++;
		}
		achievements["Platinum"].Progress = i;
	}

	public void AddProgressToAchievement(string achievementName, int progressToAdd) {
		// Additive.
		if(achievements[achievementName].IsUnlocked() == false)
			achievements[achievementName].Progress += progressToAdd;
		//SaveAchievements();
	}

	void Start () {
		Instance = this;
		print ("Achieve initializing?");
		achievements = new Dictionary<string, AchievementInfo>();
		LoadAchievements();
	}
	
	// Update is called once per frame
	void Update () {
	}

	public Dictionary<string,AchievementInfo> GetAllAchievementInfo() {
		return achievements;
	}
	
	public int getAchieveRequirement(string achievement)
	{
		switch(achievement){
		case "ClassicCats":
			return classicCatsRequired[PlayerPrefs.GetInt ("ClassicCatsLevel")];
		case "ClassicScore":
			return classicScoreRequired[PlayerPrefs.GetInt ("ClassicScoreLevel")];
		case "TimeCats":
			return timeAttackCatsRequired[PlayerPrefs.GetInt ("TimeCatsLevel")];
		case "TimeScore":
			return timeAttackScoreRequired[PlayerPrefs.GetInt ("TimeScoreLevel")];
		case "SurvivalTime":
			return survivalTimeRequired[PlayerPrefs.GetInt ("SurvivalTimeLevel")];
		default:
			return 0;
		}
	}

	public int getAchieveCurrent(string achievement)
	{
		switch(achievement){
		case "ClassicCats":
			return highestClassicCats;
		case "ClassicScore":
			return highestClassicScore;
		case "TimeCats":
			return highestTimeCats;
		case "TimeScore":
			return highestTimeScore;
		case "SurvivalTime":
			return highestSurvivalTime;
		default:
			return 0;
		}
	}
}


