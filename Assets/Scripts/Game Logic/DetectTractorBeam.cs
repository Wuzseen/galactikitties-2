using UnityEngine;
using System.Collections;

// Due to the nature of the ball of yarn, this naively follows the ball of yarn
public class DetectTractorBeam : MonoBehaviour {
	public static bool InBeam = false;
	public bool inBeam;
	public GameObject yarnBall;
	public TractorBeamSpawner tractSpawner;
	// Use this for initialization
	void Start () {
		inBeam = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(tractSpawner.getActiveTractorBeam() != null && tractSpawner.isActive()) {
			if(Vector3.Distance(tractSpawner.getActiveTractorBeam().transform.position,transform.position) < 20f) {
				inBeam = true;
			}
			else {
				inBeam = false;
			}
		} else {
			inBeam = false;
		}
		DetectTractorBeam.InBeam = inBeam;
		transform.position = yarnBall.transform.position;
	}
}
