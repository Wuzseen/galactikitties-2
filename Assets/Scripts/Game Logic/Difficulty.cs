﻿using UnityEngine;
using System.Collections;
using System;

public enum DifficultyLevel {
	Easy,
	Medium,
	Hard
}

public class Difficulty : MonoBehaviour {
	public static DifficultyLevel ActiveDifficulty;
	// Use this for initialization
	void Awake () {
		if(PlayerPrefs.HasKey("difficulty")) {
			ActiveDifficulty = (DifficultyLevel)Enum.Parse(typeof(DifficultyLevel),PlayerPrefs.GetString("difficulty"));
			print ("Loaded diff: " + ActiveDifficulty.ToString());
		} else {
			SetDifficulty("Easy");
		}
	}

	public static void SetDifficulty(string difficulty) {
		ActiveDifficulty = (DifficultyLevel)Enum.Parse(typeof(DifficultyLevel),difficulty);
		PlayerPrefs.SetString("difficulty",difficulty);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
