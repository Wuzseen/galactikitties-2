using UnityEngine;
using System.Collections;

public class KittySpawner : MonoBehaviour {
	public GameObject magicFish, asteroid, catnip, vortex, magnet, spacepod, healthCrate;
	public Spaceship spaceship;
	public static KittySpawner spawner;
	public SpawnSide[] theSpawners;
	protected int lastSpawn;
	protected int level;
	public float spawnTime = 1f;
	protected float timer = 0f;
	protected float bigKittyChance = 5f;
	protected bool asteroidAvailable = true;
	protected float basePower;
	protected bool podOn, spaceShipOn;
	private float torqueFactor;
	//private levelObj theLevelObj;
	// Use this for initialization
	void Start () {
		torqueFactor = 200f;
		basePower = 3000f;
		spaceShipOn = false;
		podOn = false;
		level = 0;
		Spawners s = Spawners.Instance;
		theSpawners = Spawners.Instance.spawnSides.ToArray();
		spawnTime = 1f;
		lastSpawn = Random.Range(0, theSpawners.Length);
		StartCoroutine("CatSpawnUpdate");
		StartCoroutine("SecondSpawner");
		spawner=this;
	}
	
	IEnumerator CatSpawnUpdate () {
		while(true) {
			if(YarnRegister.ball.go && !YarnRegister.ball.paused && !YarnRegister.ball.levelEnding) {
				timer += Time.deltaTime;
				if(timer > spawnTime) {
					spawnKitten();
					timer = 0f;
				}
			}
			yield return 0;
		}
	}

	public void NextLevel() {
		Debug.Log ("Spawner level advanced.");
		level++;
		if(level < 2)
			basePower += 1000f;
		else
			basePower += 500f;
		//if(level == 2)
		//	StartCoroutine("PodSpawner");
//		if(level == 2) Spaceship currently is removed from the game
//			StartCoroutine("SpaceShipTimer");
	}

	GameObject randomBuff () {
		print ("Rand buff." + Application.loadedLevelName);
		if(Application.loadedLevelName == "survival") {
			print ("Fnng.");
			if(Random.Range(0f,1f) <= 1f && healthCrate != null)
				return healthCrate;
			if(Random.Range(0f,1f) > .5f)
				return catnip;
			else
				return magicFish;
		}
		if(Random.Range(0f,1f) > .5f)
			return catnip;
		else
			return magicFish;
	}
	
	IEnumerator PodSpawner () {
		podOn = true;
		float podTime, podTimer;
		podTimer = 0f;
		podTime = Random.Range(10f,18f);
		while(true) {
			podTimer += Time.deltaTime;
			if(podTimer >= podTime) {
				int spawn = Random.Range (0,4);
				SpawnSide tar = theSpawners[spawn];
				SpawnSide start;
				if(spawn == 0)
					start = theSpawners[1];
				else if(spawn == 1)
					start = theSpawners[0];
				else if(spawn == 2)
					start = theSpawners[3];
				else
					start = theSpawners[2];
				if(Random.Range(0f,1f) > .5f) {
					SpawnSide t = tar;
					tar = start;
					start = t;
				}
				/*Vector3 startPos, tarPos;
				startPos = start.position;
				startPos.z = 1;
				tarPos = tar.position;
				tarPos.z = 10;
				start.position = startPos;
				tar.position = tarPos;*/
				Vector3 spawnPosition = start.RandomPoint();
				GameObject podObject = (GameObject)Instantiate (spacepod, spawnPosition, Quaternion.identity);
				SpacePod thePod = podObject.GetComponent<SpacePod>();
				SpacePod.activePod = thePod;
				thePod.SpacePodGo(randomBuff(), spawn <= 1 ? -1 : 1);
				podTimer = 0f;
			}
			yield return 0;
		}
	}
	
	IEnumerator SpaceShipTimer () {
		spaceShipOn = true;
		float spaceTimer, spaceTime;
		spaceTimer = 0f;
		spaceTime = Random.Range(15f,30f);
		while(true) {
			spaceTimer += Time.deltaTime;
			if(spaceTimer >= spaceTime) {
				print ("SPACESHIP!");
				spaceship.target = YarnRegister.ball.transform;
				yield return new WaitForSeconds(spaceship.Begin());
				spaceTimer = 0f;
			}
			yield return 0;
		}
	}
	
	IEnumerator SecondSpawner() {
		float secondTimer, secondTime;
		secondTime = spawnTime * 2f;
		secondTimer = 0f;
		while(true) {
			secondTimer += Time.deltaTime;
			if(YarnRegister.ball) {
				if(YarnRegister.ball.go && !YarnRegister.ball.paused && !YarnRegister.ball.levelEnding) {
					if(secondTimer > secondTime) {
						spawnSpecial();
						secondTimer = 0f;	
					}
				}
			}
			yield return 0;
		}
	}
	
	public int pickSpawn() {
		int spawnVal;
		while((spawnVal = Random.Range(0, theSpawners.Length)) == lastSpawn);
		lastSpawn = spawnVal;
		return spawnVal;
	}
	
	public void fishSpawn() {
		StartCoroutine(fishySpawn());
	}
	
	protected IEnumerator fishySpawn() {
		for(int i = 0; i < 3; i++) {
			foreach(SpawnSide theSpawn in theSpawners) {
				GameObject spawnedKitten = KittyPoolerRegister.Pooler.GetKitten();
				spawnedKitten.GetComponent<Rigidbody2D>().isKinematic = false;
				Vector3 spawnPoint = theSpawn.RandomPoint();
				spawnedKitten.transform.position = spawnPoint;
				if(Random.Range (0f,100f) < bigKittyChance) {
					spawnedKitten.transform.localScale = ScaleConsts.catScale * 1.5f;
					Kitty kitKat = spawnedKitten.GetComponent<Kitty>();
					kitKat.bigKitty = true;
				}
				Vector3 direction = spawnPoint - transform.position;
				direction = -direction.normalized;
				spawnedKitten.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
				spawnedKitten.GetComponent<Rigidbody2D>().AddForce(direction * (basePower + 50 * YarnRegister.ball.totalCollected()));
				float q = Random.Range(-2f,2f);
				spawnedKitten.GetComponent<Rigidbody2D>().AddTorque(torqueFactor * q);
			}
			yield return new WaitForSeconds(.4f);
		}
	}
	
	IEnumerator asteroidTimer() {
		float asteroidTimer = Random.Range(.1f,.8f);
		float astTimer = 0f;
		asteroidAvailable = false;
		while(astTimer <= asteroidTimer){
			astTimer += Time.deltaTime;
			yield return 0;
		}
		asteroidAvailable = true;
	}
	
	protected void spawn() {
		SpawnSide theSpawn = theSpawners[pickSpawn()]; // Don't get two places in a row
		GameObject spawnedKitten = KittyPoolerRegister.Pooler.GetKitten();
		if(spawnedKitten != null) {
			spawnedKitten.GetComponent<Rigidbody2D>().isKinematic = false;
			Vector3 spawnPoint = theSpawn.RandomPoint();
			spawnedKitten.transform.position = spawnPoint;
			if(Random.Range (0f,100f) < bigKittyChance) {
				spawnedKitten.transform.localScale = spawnedKitten.transform.localScale * 1.5f;
				Kitty kitKat = spawnedKitten.GetComponent<Kitty>();
				kitKat.bigKitty = true;
			}
			Vector3 direction = spawnPoint - transform.position;
			direction = -direction.normalized;
			spawnedKitten.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
			spawnedKitten.GetComponent<Rigidbody2D>().AddForce(direction * (basePower + 50 * YarnRegister.ball.totalCollected()));
			float q = Random.Range(-2f,2f);
			spawnedKitten.GetComponent<Rigidbody2D>().AddTorque(torqueFactor * q);
		}
	}
	
	// Decides if an asteroid, fish, catnip or kitten spawns--then spawns them
	void spawnKitten() {
		float dieRoll = Random.Range (0f,100f);
		if(dieRoll < 2f) {
			spawnSpecItem (magicFish);	
		} else if(dieRoll < 10f) {
			spawnSpecItem(catnip);
		} else if(dieRoll < 15f && asteroidAvailable) {
			spawnSpecItem(asteroid);
			asteroidTimer();
		}
		spawn();
	}
	
	protected void spawnSpecItem(GameObject item) {
		SpawnSide theSpawn = theSpawners[pickSpawn()]; // Don't get two places in a row
		Vector3 spawnPoint = theSpawn.RandomPoint();
		GameObject spawnedItem = (GameObject)Instantiate(item,spawnPoint,Quaternion.identity);
		spawnedItem.transform.position = spawnPoint;
		Vector3 direction = spawnPoint - transform.position;
		direction = -direction.normalized;
		spawnedItem.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
		spawnedItem.GetComponent<Rigidbody2D>().AddForce(direction * (basePower + 50 * YarnRegister.ball.totalCollected()));
		float q = Random.Range(-2f,2f);
		spawnedItem.GetComponent<Rigidbody2D>().AddTorque(torqueFactor * q);
	}
	
	void spawnVortex() {
		SpawnSide theSpawn = theSpawners[pickSpawn()];
		Vector3 spawnPoint = theSpawn.RandomPoint();
		GameObject spawnedItem = (GameObject)Instantiate(vortex,spawnPoint,Quaternion.identity);
		spawnedItem.transform.position = spawnPoint;
		spawnedItem.transform.position = spawnPoint;
		Vector3 direction = spawnPoint - transform.position;
		direction = -direction.normalized;
		spawnedItem.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
		spawnedItem.GetComponent<Rigidbody2D>().AddForce(direction * (4000f + 50 * YarnRegister.ball.totalCollected()));
		
	}
	
	void spawnSpecial() {
		float dieRoll = Random.Range (0f,100f);
		if(Application.loadedLevelName == "survival") {
			float die2 = Random.Range(0f,100f);
			if(die2 < 15f) {
				spawnSpecItem(healthCrate);
			}
		}
		if(level == 0) {
			if(dieRoll < 5f) {
				spawnSpecItem (magicFish);	
			} else if(dieRoll < 20f) {
				spawnSpecItem(catnip);
			} else if(dieRoll < 50f) {
				spawnSpecItem(asteroid);
			}
		} else if (level == 1) {
			if(dieRoll < 5f) {
				spawnSpecItem (magicFish);	
			} else if(dieRoll < 20f) {
				spawnSpecItem(catnip);
			} else if(dieRoll < 35f) {
				spawnVortex();
			} else if(dieRoll < 65f) {
				spawnSpecItem(asteroid);
			}
		} else {
			if(dieRoll < 5f) {
				spawnSpecItem (magicFish);	
			} else if(dieRoll < 15f) {
				spawnVortex();
			} else if(dieRoll < 30f) {
				spawnSpecItem(catnip);
			} else if(dieRoll < 50f) {
				spawnSpecItem(magnet);
			} else if(dieRoll < 80f) {
				spawnSpecItem(asteroid);
			}
		}
	}
	
	public void MagnetAttack() {
		StartCoroutine("AsteroidsGo");
	}
	
	IEnumerator AsteroidsGo() {
		for(int i = 0; i < 1; i++) {
			foreach(SpawnSide theSpawn in theSpawners) {
				Vector3 spawnPoint = theSpawn.RandomPoint();
				GameObject spawnedObject = (GameObject)Instantiate(asteroid,spawnPoint,Quaternion.identity);
				spawnedObject.transform.position = spawnPoint;
				Vector3 direction = spawnPoint - transform.position;
				direction = -direction.normalized;
				spawnedObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
				spawnedObject.GetComponent<Rigidbody2D>().AddForce(direction * (basePower + 50 * YarnRegister.ball.totalCollected()));
				float q = Random.Range(-2f,2f);
				spawnedObject.GetComponent<Rigidbody2D>().AddTorque(torqueFactor * q);
			}
			yield return new WaitForSeconds(.4f);
		}
	}
}
