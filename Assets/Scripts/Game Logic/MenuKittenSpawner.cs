﻿using UnityEngine;
using System.Collections;

public class MenuKittenSpawner : MonoBehaviour {
	public GameObject magicFish, asteroid, catnip;
	public SpawnSide[] theSpawners;
	private int lastSpawn;
	public float spawnTime = 1f;
	private float timer = 0f;
	private float bigKittyChance = 5f;
	private bool asteroidAvailable = true;
	public bool debug = false;
	private float torqueFactor;
	//private levelObj theLevelObj;
	// Use this for initialization
	void Start () {
		torqueFactor = 200f;
		Spawners s = Spawners.Instance;
		theSpawners = s.spawnSides.ToArray();
		if(debug)
			Destroy(this);
		spawnTime = 1f;
		lastSpawn = Random.Range(0, theSpawners.Length);
		if(GoogleAnalytics.instance) {
			GoogleAnalytics.instance.LogScreen("Main Menu");
		}
		StartCoroutine("SecondSpawner");
	}
	
	IEnumerator SecondSpawner() {
		float secondTimer, secondTime;
		secondTime = spawnTime * 2f;
		secondTimer = 0f;
		while(true) {
			secondTimer += Time.deltaTime;
			if(secondTimer > secondTime) {
				spawnSpecial();
				secondTimer = 0f;	
			}
			yield return 0;
		}
	}
	
	// Update is called once per frame
	void Update () {
			timer += Time.deltaTime;
			if(timer > spawnTime) {
				spawnKitten();
				timer = 0f;
			}
	}
	
	public int pickSpawn() {
		int spawnVal;
		while((spawnVal = Random.Range(0, theSpawners.Length)) == lastSpawn);
		lastSpawn = spawnVal;
		return spawnVal;
	}
	
	public void fishSpawn() {
		StartCoroutine(fishySpawn());
	}
	
	IEnumerator fishySpawn() {
		for(int i = 0; i < 3; i++) {
			foreach(SpawnSide theSpawn in theSpawners) {
				Vector3 spawnPoint = theSpawn.RandomPoint();
				GameObject spawnedKitten = KittyPoolerRegister.Pooler.GetKitten();
				spawnedKitten.transform.position = spawnPoint;
				if(Random.Range (0f,100f) < bigKittyChance) {
					spawnedKitten.transform.localScale = spawnedKitten.transform.localScale * 1.5f;
					Kitty kitKat = spawnedKitten.GetComponent<Kitty>();
					kitKat.bigKitty = true;
				}
				Vector3 direction = spawnPoint - transform.position;
				direction = -direction.normalized;
				spawnedKitten.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
				spawnedKitten.GetComponent<Rigidbody2D>().AddForce(direction * 5000f);
				float q = Random.Range(-2f,2f);
				spawnedKitten.GetComponent<Rigidbody2D>().AddTorque(torqueFactor * q);
			}
			yield return new WaitForSeconds(.4f);
		}
	}
	
	IEnumerator asteroidTimer() {
		float asteroidTimer = Random.Range(.1f,.8f);
		float astTimer = 0f;
		asteroidAvailable = false;
		while(astTimer <= asteroidTimer){
			astTimer += Time.deltaTime;
			yield return 0;
		}
		asteroidAvailable = true;
	}

	void spawn() {
		SpawnSide theSpawn = theSpawners[pickSpawn()]; // Don't get two places in a row
		if(theSpawn == null)
			return; // Can happen in the beginning if you're in editor
		Vector3 spawnPoint = theSpawn.RandomPoint();
		GameObject spawnedKitten = KittyPoolerRegister.Pooler.GetKitten();
		spawnedKitten.transform.position = spawnPoint;
		if(Random.Range (0f,100f) < bigKittyChance) {
			spawnedKitten.transform.localScale = spawnedKitten.transform.localScale * 1.5f;
			Kitty kitKat = spawnedKitten.GetComponent<Kitty>();
			kitKat.bigKitty = true;
		}
		Vector3 direction = spawnPoint - transform.position;
		direction = -direction.normalized;
		spawnedKitten.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
		spawnedKitten.GetComponent<Rigidbody2D>().AddForce(direction * 5000f);
		float i = Random.Range(-2f,2f);
		spawnedKitten.GetComponent<Rigidbody2D>().AddTorque(torqueFactor * i);
	}
	
	// Decides if an asteroid, fish, catnip or kitten spawns--then spawns them
	void spawnKitten() {
		float dieRoll = Random.Range (0f,100f);
		if(dieRoll < 2f) {
			spawnSpecItem (magicFish);	
		} else if(dieRoll < 10f) {
			spawnSpecItem(catnip);
		} else if(dieRoll < 15f && asteroidAvailable) {
			spawnSpecItem(asteroid);
			asteroidTimer();
		}
		spawn();
	}
	
	void spawnSpecItem(GameObject item) {
		SpawnSide theSpawn = theSpawners[pickSpawn()]; // Don't get two places in a row
		Vector3 spawnPoint = theSpawn.RandomPoint();
		GameObject spawnedItem = (GameObject)Instantiate(item,spawnPoint,Quaternion.identity);
		spawnedItem.transform.position = spawnPoint;
		Vector3 direction = spawnPoint - transform.position;
		direction = -direction.normalized;
		spawnedItem.GetComponent<Rigidbody2D>().velocity = new Vector3(0,0,0);
		spawnedItem.GetComponent<Rigidbody2D>().AddForce(direction * 5000f);
		float i = Random.Range(-2f,2f);
		spawnedItem.GetComponent<Rigidbody2D>().AddTorque(torqueFactor * i);
	}
	
	void spawnSpecial() {
		float dieRoll = Random.Range (0f,100f);
		if(dieRoll < 5f) {
			spawnSpecItem (magicFish);	
		} else if(dieRoll < 15f) {
			spawnSpecItem(asteroid);
		} else if(dieRoll < 30f) {
			spawnSpecItem(catnip);
		}
	}
	
	void OnDestroy() {
		KittyPoolerRegister.Pooler.PoolAll();	
	}
}