﻿using UnityEngine;
using System.Collections;


// Tractor beams are how the kittens are collected
// They spawn every 15-20 seconds
// Power ups can decrease the time to spawn, or increase time alive
// They last 5 seconds
public class TractorBeamSpawner : MonoBehaviour {
	public TractorBeamRateHandler guiBeamRates;
	public GameObject tractorBeamObject;
	public YarnBall theYarn;
	public float timeLimit;
	public float lowerTime, upperTime; // Range of time limits
	private GameObject activeTractorBeam = null;
	private bool _isActive = false;
	private bool timing = true;
	private bool hasWarned = false;
	public float timer;
	private float xLowLim, yLowLim, xHighLim, yHighLim, zPos;
	
	// Use this for initialization
	void Start () {
		pickNewTimeLimit();
		guiBeamRates.StartOn(timeLimit);
		zPos = 10;
		xLowLim = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * .2f,0f,0f)).x;
		yLowLim = Camera.main.ScreenToWorldPoint(new Vector3(0f,Screen.height * .2f,0f)).y;
		xHighLim = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * .8f,0f,0f)).x;
		yHighLim = Camera.main.ScreenToWorldPoint(new Vector3(0f,Screen.height * .8f,0f)).y;
	}
	
	public void Reset() {
		timing = false;
		timer = 0f;
	}

	public void TutorialShutOff() {

	}

	public void TutorialResume() {

	}
	
	public void Go() {
		timing = true;
	}
	
	// Picks between 15 and 20 seconds until the next tractor beam
	// Also resets the timer
	private void pickNewTimeLimit() {
		timeLimit = Random.Range(lowerTime,upperTime);
		timer = 0f;
	}
	
	public GameObject getActiveTractorBeam() {
		if(activeTractorBeam != null)
			return activeTractorBeam;
		return null;
	}
	
	IEnumerator tractorTimer() {
		while(timer >= 0f && theYarn.go && !theYarn.paused) {
			timer -= Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		pickNewTimeLimit();
		guiBeamRates.StartOn(timeLimit);
		deactivate();
	}
	
	public void activate() {
		guiBeamRates.StartOff(timer);
		_isActive = true;
		float xPos = Random.Range(xLowLim, xHighLim);
		float yPos = Random.Range(yLowLim, yHighLim);
		activeTractorBeam = (GameObject)Instantiate(tractorBeamObject, new Vector3(xPos, yPos, zPos), Quaternion.identity);
		soundEffects.tractorBeam();
		StartCoroutine(tractorTimer());
		if(!hasWarned) {
			Warning.Alert("Get the ball in the portal!");
			hasWarned = true;
		}
	}
	
	public void deactivate() {
		_isActive = false;
		activeTractorBeam.GetComponent<ParticleSystem>().enableEmission = false;
		Destroy (activeTractorBeam,2f);
	}
	
	public bool isActive() {
		return _isActive;	
	}
	
	// Update is called once per frame
	void Update () {
		if(!isActive() && theYarn.go && !theYarn.paused && timing)
			timer += Time.deltaTime;
		
		if(timer >= timeLimit && timing) {
			activate();
			timer = timeLimit * .65f;
		}
	}
}