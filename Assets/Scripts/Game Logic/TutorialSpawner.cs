﻿using UnityEngine;
using System.Collections;

public class TutorialSpawner : KittySpawner {
	// Might need to be extended some day
	class TutorialMessage {
		private string prompt;
		public string Prompt {
			get { return prompt; }
			set { prompt = value; }
		}
		private float length;
		public float TimeLength {
			get { return length; }
		}
				
		public TutorialMessage(string aPrompt) {
			prompt = aPrompt;
			length = aPrompt.Length * .04f;
		}

		public void UpdatePrompt(string newPrompt) {
			prompt = newPrompt;
			length = newPrompt.Length * .04f;
		}
	}

	public UILabel tutorialPrompt;
	public TractorBeamSpawner tractorBeamSpawner;
	private TutorialYarn yarn;
	private bool printing;

	void Start () {
		if(GoogleAnalytics.instance) {
			GoogleAnalytics.instance.LogScreen("Tutorial");
			print ("Tut logged");
		}
		yarn = (TutorialYarn)YarnRegister.ball;
		basePower = 3000f;
		spaceShipOn = false;
		podOn = false;
		level = 0;
		Spawners s = Spawners.Instance;
		theSpawners = s.spawnSides.ToArray(); 
		spawnTime = 1f;
		lastSpawn = Random.Range(0, theSpawners.Length);
		StartCoroutine("BeginTutorial");
		spawner=this;
	}

	IEnumerator BeginTutorial () {
		yarn.waitOnTutorial = true;
		TutorialMessage startPrompt = new TutorialMessage(
			"Welcome To Galactikitties\n\n" +
			"You can quit the tutorial with the\n" +
			"pause button in the bottom left\n"
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 2.8f);
		startPrompt.UpdatePrompt(
			"Galactikitties is about rescuing\n" +
			"cats in space.\n\n" +
			"Tap to move your ball of yarn"
			);
		StartCoroutine("Play",startPrompt);
		yarn.waitOnTutorial = false;
		while(true) { // Waits for user to tap screen somewhere
			if((Input.touchCount > 0 || Input.GetMouseButton(0)) && printing == false)
				break;
			yield return 0;
		}
		yield return new WaitForSeconds(.6f);
		yarn.waitOnTutorial = true;
		startPrompt.UpdatePrompt(
			"Catch Cats with the ball of yarn.\n\n" +
			"Try getting 5 on the ball now."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength);
		while(yarn.collected.Count < 5) {
			spawn();
			yield return new WaitForSeconds(1f);
		}
		yarn.go = true;
		startPrompt.UpdatePrompt(
			"Watch the portal timer at the bottom"
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(tractorBeamSpawner.timeLimit * .8f);
		yarn.go = false;
		startPrompt.UpdatePrompt(
			"When this timer fills a portal spawns.\n\n" +
			"Get the ball into the portal\n" +
			"to rescue your cats!"
			);
		StartCoroutine("Play",startPrompt);
		int currentCount = yarn.collected.Count;
		yield return new WaitForSeconds(startPrompt.TimeLength);
		yarn.go = true;
		while(currentCount == yarn.collected.Count)
			yield return 0;
		startPrompt.UpdatePrompt(
			"Each cat is worth a point.\n\n" +
			"Watch out for Big cats\nthey are worth two points!\n"
			);
		StartCoroutine("Play",startPrompt);
		while(yarn.collected.Count > 0)
			yield return 0;
		yield return new WaitForSeconds(7f);
		startPrompt.UpdatePrompt(
			"The portal only appears occasionally.\n" +
			"So keep an eye on the portal timer!"
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 3f);
		yarn.go = false;
		startPrompt.UpdatePrompt(
			"The top right shows you how many\n" +
			"cats are required to beat the current level."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 3f);
		startPrompt.UpdatePrompt(
			"Not every mode has a portal\n\n" +
			"In Time Attack mode you only have to collect cats."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 3f);
		startPrompt.UpdatePrompt(
			"Collect the required amount of cats before\n" +
			"the timer at the top hits 0 to advance levels."
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(startPrompt.TimeLength + 3f);

		startPrompt.UpdatePrompt(
			"Things like catnip and fish will help you out\n" +
			"\n" +
			"Fish will spawn lots of cats at once but is quite rare!"
			);
		StartCoroutine("Play",startPrompt);
		while(yarn.collected.Count < 20) {
			spawnSpecItem (magicFish);	
			yield return new WaitForSeconds(2.5f);
		}
		yield return new WaitForSeconds(2f);
		
		startPrompt.UpdatePrompt(
			"Catnip speeds up the rotation of the ball\n" +
			"It also causes Cats to spawn twice as often!\n" +
			"Catnip only lasts a few seconds,\n collecting multiple extends the effect!"
			);
		StartCoroutine("Play",startPrompt);
		int spawnedCatnip = 0;
		while(spawnedCatnip < 5) {
			spawnSpecItem (catnip);
			spawnedCatnip ++;
			yield return new WaitForSeconds(2f);
		}
		yield return new WaitForSeconds(2f);
		
		startPrompt.UpdatePrompt(
			"Watch out for asteroids and satellites!\n\n" +
			"Don't let your cats get hit!"
			);
		StartCoroutine("Play",startPrompt);
		int spawnedAsteroids = 0;
		while(spawnedAsteroids < 8) {
			spawnSpecItem(asteroid);
			spawnSpecItem(asteroid);
			spawnedAsteroids++;
			yield return new WaitForSeconds(1.5f);
		}
		yield return new WaitForSeconds(2f);
		
		startPrompt.UpdatePrompt(
			"Watch out for stray black holes!"
			);
		StartCoroutine("Play",startPrompt);
		int spawnedHoles = 0;
		while(spawnedHoles < 8) {
			spawnSpecItem(vortex);
			spawnedHoles++;
			yield return new WaitForSeconds(.5f);
		}
		yield return new WaitForSeconds(2f);
		
		startPrompt.UpdatePrompt(
			"Unlock customization for your cats\nby completing achievements!"
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(4f);
		
		startPrompt.UpdatePrompt(
			"Get past the first few levels to encounter\n" +
			"dangerous space magnets!"
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(6f);
		
		startPrompt.UpdatePrompt(
			"Now go save some cats!\n"
			);
		StartCoroutine("Play",startPrompt);
		yield return new WaitForSeconds(4f);
		Achievements.Instance.AddProgressToAchievement("Smart Cat",1);
		Achievements.Instance.AddProgressToAchievement("Silly Cat",1);
		Application.LoadLevel("mainMenu");

		tutorialPrompt.text = "";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	IEnumerator Play(TutorialMessage aMessage) {
		printing = true;
		int count = aMessage.Prompt.Length;
		float timeLimit = aMessage.TimeLength;
		float time = 0.0f;
		while(time <= timeLimit) {
			int numofchars = (int)((float)count * (time/timeLimit));
			time += Time.deltaTime;
			tutorialPrompt.text = aMessage.Prompt.Substring(0,numofchars);
			yield return 0;
		}
		printing = false;
		tutorialPrompt.text = aMessage.Prompt;
	}
}
