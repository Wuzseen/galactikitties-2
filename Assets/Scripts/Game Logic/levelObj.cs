using UnityEngine;
using System.Collections;

public class levelObj : MonoBehaviour {
	private int mode = 0;
	private int level = 1;
	private int score = 0;
	private bool willResetScore = true;
	public string score1Num, score2Num, score3Num, score4Num, score5Num,
				score1Name, score2Name, score3Name, score4Name, score5Name;
	public bool scoresSet;
	private string addScoreURL = "http://www.travistchandler.com/php/addscore.php?"; //be sure to add a ? to your url
    private string highscoreURL = "http://www.travistchandler.com/php/display.php";
	private string addUserURL = "http://travistchandler.com/php/CreateLogin.php?"; //be sure to add a ? to your url
	private string loginURL = "http://travistchandler.com/php/SendLogin.php?";
	private string post_url;
	public bool loggedIn = false;
	public string loggedUser;
	private static levelObj inst;
	// Use this for initialization
	void Awake () {
		if(inst != null) {
			Destroy(this.gameObject);
			return;
		}
		inst = this;
		DontDestroyOnLoad(this.gameObject);
	}
	
	public void addScore(int theAdd) {
		score = theAdd;
	}
	
	public int getMode() {
		return mode;	
	}
	
	public void setMode(int _mode) {
		mode = _mode;	
	}
	
	public int getScore() {
		return score;
	}
	
	public void newLevel() {
		if(KittySpawner.spawner != null)
			KittySpawner.spawner.NextLevel();

		level++;
		if(level >= 5)
			Achievements.Instance.AddProgressToAchievement("The Purrfesional",1);
	}
	
	public int requiredKittens() {
		float tLFactor = 1f;
		if(Difficulty.ActiveDifficulty == DifficultyLevel.Easy) {
			tLFactor = .6f;
		} else if(Difficulty.ActiveDifficulty == DifficultyLevel.Medium) {
			tLFactor = .8f;
		}
		float tL = (float)timeLimit();
		return (int)(tL * tLFactor) + (int)Mathf.Pow(2,level + 1);
	}
	
	public int survivalKittens() {
		int b = 10 * level;
		if(Difficulty.ActiveDifficulty == DifficultyLevel.Easy) {
			b = 5 * level;
		} else if(Difficulty.ActiveDifficulty == DifficultyLevel.Medium) {
			b = 8 * level;
		}
		return (level * level / 2) + b;	
	}
	
	public int survivalTime() {
		//return 5;
		return 20 + 5 * level;	
	}
	
	public int timeLimit() {
		return 40 + 20 * level;	
	}
	
	public int getLevel() {
		return level;
	}
	  
 
    // remember to use StartCoroutine when calling this function!
    public IEnumerator submitData(string name, string password)
    {
        //This connects to a server side php script that will add the name and score to a MySQL DB.
        // Supply it with a string representing the players name and the players score.
 
        post_url = addUserURL + "name=" + WWW.EscapeURL(name) + "&password=" + password;
 
        // Post the URL to the site and create a download object to get the result.
        WWW hs_post = new WWW(post_url);
        yield return hs_post; // Wait until the download is done
 		print (hs_post.text);
		if(hs_post.text == "User created"){
			loggedIn = true;
			loggedUser = name;
			print("User logged in");
			PlayerPrefs.SetString ("UserName", name);
			PlayerPrefs.SetString ("Password", password);
			PlayerPrefs.Save();
		}	
        if (hs_post.error != null)
        {
            print("There was an error posting the high score: " + hs_post.error);
        }
    }
	public IEnumerator checkData(string name, string password)
	{
        
		
		post_url = loginURL + "name=" + WWW.EscapeURL(name) + "&password=" + password;
		WWW hs_post = new WWW(post_url);
		yield return hs_post;
		print ("Data checked");
		if(hs_post.text == "Session started"){
				loggedIn = true;
				loggedUser = name;
				print("User logged in");
			PlayerPrefs.SetString ("UserName", name);
			PlayerPrefs.SetString ("Password", password);
			PlayerPrefs.Save();
		}
		if(hs_post.text == "Invalid"){
			print ("Error loggin in, invalid username or password");
			if(Application.loadedLevel == 1){
				Application.LoadLevel("loginScreen");	
			}
		}
		
		if(hs_post.error != null)
		{
			print ("There was an error in checking log in:" + hs_post.error);
			if(Application.loadedLevel == 1){
				Application.LoadLevel("mainMenu");	
			}
		}
		
	}
	 public IEnumerator PostScores(string name, int score)
    {
		Debug.Log ("Posting Scores");
        //This connects to a server side php script that will add the name and score to a MySQL DB.
        // Supply it with a string representing the players name and the players score.
 
        string post_url = addScoreURL + "name=" + WWW.EscapeURL(name) + "&score=" + score;
 		print (post_url);
        // Post the URL to the site and create a download object to get the result.
        WWW hs_post = new WWW(post_url);
        yield return hs_post; // Wait until the download is done
		
 		StartCoroutine(GetScores());
		
        if (hs_post.error != null)
        {
            print("There was an error posting the high score: " + hs_post.error);
        }
    }
 
    // Get the scores from the MySQL DB to display in a GUIText.
    // remember to use StartCoroutine when calling this function!
    public IEnumerator GetScores()
    {
		Debug.Log("Getting Scores");
        WWW hs_get = new WWW(highscoreURL);
        yield return hs_get;
 
        if (hs_get.error != null)
        {
            print("There was an error getting the high score: " + hs_get.error);
        }
        else
        {
            string retrievedScores = hs_get.text; //Pull string for scores
			
			int lineEndLocation = retrievedScores.IndexOf("\n");
			int lineTabLocation = retrievedScores.IndexOf ("\t");
			score1Name = retrievedScores.Substring(0, lineTabLocation);
			score1Num = retrievedScores.Substring(lineTabLocation + 1, lineEndLocation - lineTabLocation - 1);
			if(retrievedScores.Length > lineEndLocation + 3){
				int lineEndLocation2 = retrievedScores.IndexOf("\n", lineEndLocation + 1);
				int lineTabLocation2 = retrievedScores.IndexOf ("\t", lineTabLocation + 1);
				score2Name = retrievedScores.Substring(lineEndLocation + 1, lineTabLocation2 - lineEndLocation - 1);
				score2Num = retrievedScores.Substring(lineTabLocation2 + 1, lineEndLocation2 - lineTabLocation2 - 1);
				if(retrievedScores.Length > lineEndLocation2 + 3){
					int lineEndLocation3 = retrievedScores.IndexOf("\n", lineEndLocation2 + 1);
					int lineTabLocation3 = retrievedScores.IndexOf ("\t", lineTabLocation2 + 1);
					score3Name = retrievedScores.Substring(lineEndLocation2 + 1, lineTabLocation3 - lineEndLocation2 - 1);
					score3Num = retrievedScores.Substring(lineTabLocation3 + 1, (lineEndLocation3 - lineTabLocation3 - 1));
					if(retrievedScores.Length > lineEndLocation3 + 3){
						int lineEndLocation4 = retrievedScores.IndexOf("\n", lineEndLocation3 + 1);
						int lineTabLocation4 = retrievedScores.IndexOf ("\t", lineTabLocation3 + 1);
						score4Name = retrievedScores.Substring(lineEndLocation3 + 1, lineTabLocation4 - lineEndLocation3 - 1);
						score4Num = retrievedScores.Substring(lineTabLocation4 + 1, (lineEndLocation4 - lineTabLocation4 - 1));
						if(retrievedScores.Length > lineEndLocation4 + 3){
							int lineEndLocation5 = retrievedScores.IndexOf("\n", lineEndLocation4 + 1);
							int lineTabLocation5 = retrievedScores.IndexOf ("\t", lineTabLocation4 + 1);
							score5Name = retrievedScores.Substring(lineEndLocation4 + 1, lineTabLocation5 - lineEndLocation4 - 1);
							score5Num = retrievedScores.Substring(lineTabLocation5 + 1, lineEndLocation5 - lineTabLocation5 - 1);
						}
					}
				}
			}
			scoresSet = true;
        }
    }

	// Update is called once per frame
	void Update () {
		if(Application.loadedLevelName != "classic" || Application.loadedLevelName != "survival")
			willResetScore = true;
		if(Application.loadedLevelName == "mainMenu" && willResetScore) {
			score = 0;
			willResetScore = false;
			level = 1;
		}
	}
}
