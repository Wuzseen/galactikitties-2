﻿using UnityEngine;
using System.Collections;

public class Bubble : MonoBehaviour {
    public Transform target;
    public GoEaseType easingType;

	// Use this for initialization
	void Start () {
        GoTweenConfig cfg = new GoTweenConfig();
        cfg.position(target.position);
        cfg.setEaseType(easingType);
        Go.to(this.gameObject.transform, 2f + Random.Range(-.3f,.3f), cfg);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
