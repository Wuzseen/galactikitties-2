using UnityEngine;
using System.Collections;

public class Catnip : MonoBehaviour {
	public float zAxisClamp = 10f;
	// Use this for initialization
	void Start () {
		Destroy (this.gameObject,15f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(transform.position.x,transform.position.y,zAxisClamp);
	
	}
	
	void OnCollisionEnter(Collision collision) {
		if((collision.transform.tag == "kitty" && collision.transform.parent != null && Application.loadedLevelName != "mainMenu") || collision.transform.tag == "yarnBall") {
			YarnRegister.ball.addCatnip();
			BorderPulser.border.Good();
			Destroy (this.gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if((collision.transform.tag == "kitty" && collision.transform.parent != null && Application.loadedLevelName != "mainMenu") || collision.transform.tag == "yarnBall") {
			YarnRegister.ball.addCatnip();
			soundEffects.Instance.randomCollect();
			BorderPulser.border.Good();
			Achievements.Instance.AddProgressToAchievement("Catnip Dealer",1);
			YarnRegister.ball.ninjaTime = 0f;
			Destroy (this.gameObject);
		}
	}
}
