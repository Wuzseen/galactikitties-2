﻿using UnityEngine;
using System.Collections;

public class HealthCrate : MonoBehaviour {
	private SurvivalYarn yarn;
	public int HealAmount = 2;
	// Use this for initialization
	void Start () {
		yarn = (SurvivalYarn)YarnRegister.ball;
	}

	void Heal() {
		yarn.HPPlus(HealAmount);
		soundEffects.Instance.randomCollect();
		BorderPulser.border.Good();
		Achievements.Instance.AddProgressToAchievement("Like a Vet",1);
		Destroy (this.gameObject);
	}
	
	void OnCollisionEnter2D(Collision2D collision) {
		if((collision.transform.tag == "kitty" && collision.transform.parent != null && Application.loadedLevelName != "mainMenu") || collision.transform.tag == "yarnBall") {
			if(collision.transform.tag == "yarnBall") {
				Heal();
				return;
			}
			Kitty k = collision.gameObject.GetComponent<Kitty>();
			if(k.rescued) {
				Heal();
			}
		}
	}
}
