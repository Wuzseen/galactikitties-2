using UnityEngine;
using System.Collections;

public class fish : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy (this.gameObject,15f);
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.rotation.x > 0f || transform.rotation.x < 0f) {
			Quaternion temp = transform.rotation;
			temp.x = 0f;
			transform.rotation = temp;
		}
		if(transform.position.z != 10f) {
			Vector3 temp = transform.position;
			temp.z = 10f;
			transform.position = temp;
		}
	}
	
	void OnCollisionEnter(Collision collision) {
		if((collision.transform.tag == "kitty" && collision.transform.parent != null && Application.loadedLevelName != "mainMenu") || collision.transform.tag == "yarnBall") {
			KittySpawner spawner = SpawnRegister.spawn.GetComponent<KittySpawner>();
			soundEffects.Instance.randomCollect();
			spawner.fishSpawn();
			BorderPulser.border.Good();
			Destroy (this.gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if((collision.transform.tag == "kitty" && collision.transform.parent != null && Application.loadedLevelName != "mainMenu") || collision.transform.tag == "yarnBall") {
			KittySpawner spawner = SpawnRegister.spawn.GetComponent<KittySpawner>();
			soundEffects.Instance.randomCollect();
			spawner.fishSpawn();
			BorderPulser.border.Good();
			Achievements.Instance.AddProgressToAchievement("Fish Fiend",1);
			YarnRegister.ball.ninjaTime = 0f;
			Destroy (this.gameObject);
		}
	}
}
