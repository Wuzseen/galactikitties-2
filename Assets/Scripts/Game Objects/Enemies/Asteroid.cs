using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour {
	public GameObject miniSteroids;
	private bool dying = false;
	private string[] skins = new string[]{"asteroid","Asteroid2","Satellite"};
	// Use this for initialization
	void Start () {
		Destroy (this.gameObject,15f);
		tk2dSprite sprite = GetComponent<tk2dSprite>();
		sprite.spriteId = sprite.Collection.GetSpriteIdByName(skins[Random.Range(0,skins.Length)]);
		this.transform.localScale *= Random.Range(.8f,1.2f);
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.rotation.x > 0f || transform.rotation.x < 0f) {
			Quaternion temp = transform.rotation;
			temp.x = 0f;
			transform.rotation = temp;
		}
		if(transform.position.z != 10f) {
			Vector3 temp = transform.position;
			temp.z = 10f;
			transform.position = temp;
		}
		if(GetComponent<Rigidbody2D>().velocity.magnitude < 50f)
			GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * 50f;
	}
	
	IEnumerator DeathToAsteroid() {
		dying = true;	
		float start = 0f;
		float timer = 3f; // Lifetime before mini asteroids
		while(start < timer) {
			start += Time.deltaTime;	
			yield return 0;
		}
		GameObject roids = (GameObject)Instantiate (miniSteroids,transform.position,Quaternion.identity);
		Destroy (roids,3f);
		Destroy (this.gameObject);
	}
	
	void OnCollisionEnter(Collision collision) {
		if(collision.transform.tag == "kitty") {
			if(DetectTractorBeam.InBeam) {
				return;
			}
			Camera.main.GetComponent<CameraShake>().Shake(5f,.3f);
			soundEffects.randomMeow();
			soundEffects.randomAsteroid();
			if(YarnRegister.ball != null) {
				YarnRegister.ball.ScatterTheKittens(collision.transform);
				BorderPulser.border.Bad();
			}
		}
		if (collision.transform.tag == "yarnBall" && Application.loadedLevelName == "survival") {
			if(DetectTractorBeam.InBeam) {
				return;
			}
            soundEffects.randomAsteroid();
            SurvivalYarn yrn = collision.gameObject.GetComponent<SurvivalYarn>();
            yrn.Lose();
        }
		if(!dying)
			StartCoroutine("DeathToAsteroid");
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.transform.tag == "kitty") {
			soundEffects.randomAsteroid();
			if(DetectTractorBeam.InBeam && Difficulty.ActiveDifficulty != DifficultyLevel.Hard) {
				return;
			}
			Camera.main.GetComponent<CameraShake>().Shake(5f,.3f);
			soundEffects.randomMeow();
			if(YarnRegister.ball != null) {
				YarnRegister.ball.ScatterTheKittens(collision.transform);
				BorderPulser.border.Bad();
			}
		}
		if (collision.transform.tag == "yarnBall" && Application.loadedLevelName == "survival") {
			soundEffects.randomAsteroid();
			if(DetectTractorBeam.InBeam && Difficulty.ActiveDifficulty != DifficultyLevel.Hard) {
				return;
			}
//			SurvivalYarn yrn = collision.gameObject.GetComponent<SurvivalYarn>();
//			yrn.Lose();
		}
		if(collision.transform.tag == "yarnBall")
			YarnRegister.ball.ninjaTime = 0f;
		if(!dying)
			StartCoroutine("DeathToAsteroid");
	}
}
