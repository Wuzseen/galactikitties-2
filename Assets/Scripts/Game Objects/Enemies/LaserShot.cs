﻿using UnityEngine;
using System.Collections;

public class LaserShot : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	void OnCollisionEnter(Collision collision) {
		if(collision.transform.tag == "kitty") {
			Camera.main.GetComponent<CameraShake>().Shake(5f,.3f);
			soundEffects.randomMeow();
			soundEffects.randomAsteroid(); // Should be a laser shot
			if(YarnRegister.ball != null) {
				YarnRegister.ball.ScatterTheKittens(collision.transform);
				BorderPulser.border.Bad();
			}
		}
        if (collision.transform.tag == "yarnBall" && Application.loadedLevelName == "survival") {
            soundEffects.randomAsteroid();
            SurvivalYarn yrn = collision.gameObject.GetComponent<SurvivalYarn>();
            yrn.Lose();
        }
		Destroy (this.gameObject);
	}

	void OnCollisionEnter2D(Collision2D collision) {
		Debug.Log("Laser hit!.");
		if(collision.transform.tag == "kitty") {
			Camera.main.GetComponent<CameraShake>().Shake(5f,.3f);
			soundEffects.randomMeow();// Should be a laser shot
			if(YarnRegister.ball != null) {
				YarnRegister.ball.ScatterTheKittens(collision.transform);
				BorderPulser.border.Bad();
			}
		}
		if (collision.transform.tag == "yarnBall" && Application.loadedLevelName == "survival") {
			soundEffects.randomAsteroid();
			SurvivalYarn yrn = collision.gameObject.GetComponent<SurvivalYarn>();
			yrn.Lose();
		}
		Destroy (this.gameObject);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "kitty") {
			Camera.main.GetComponent<CameraShake>().Shake(5f,.3f);
			soundEffects.randomMeow();// Should be a laser shot
			if(YarnRegister.ball != null) {
				YarnRegister.ball.ScatterTheKittens(other.transform);
				BorderPulser.border.Bad();
			}
		}
		if (other.transform.tag == "yarnBall" && Application.loadedLevelName == "survival") {
			SurvivalYarn yrn = other.gameObject.GetComponent<SurvivalYarn>();
			yrn.Lose();
		}
		Destroy (this.gameObject);
	}
	
	void OnDeath() {
    	ParticlePoolerRegister.Pooler.GetPoof().transform.position = transform.position;	
	}
}
