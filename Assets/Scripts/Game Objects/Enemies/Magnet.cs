﻿using UnityEngine;
using System.Collections;

public class Magnet : MonoBehaviour {
	private bool killMeNow;
	// Use this for initialization
	void Start () {
		killMeNow = false;
		StartCoroutine("BecomerOfDeath");
	}
	
	IEnumerator BecomerOfDeath() {
		float timer = 0f;
		while (timer < 15f && killMeNow == false) {
			timer += Time.deltaTime;
			yield return 0;
		}
		
		StartCoroutine("DeathSequence");
	}
	
	IEnumerator DeathSequence () {
		GetComponent<Animation>().Play("magnetDeath");
		yield return new WaitForSeconds( GetComponent<Animation>()["magnetDeath"].length );
		Destroy(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnCollisionEnter(Collision collision) {
		bool rescuedKitty = false;
		if(collision.transform.tag == "kitty") {
			Kitty k = collision.gameObject.GetComponent<Kitty>();
			rescuedKitty = k.rescued;
		}
        if (collision.transform.tag == "yarnBall" || rescuedKitty) {
            soundEffects.randomMagnet();
			SpawnRegister.spawn.MagnetAttack();	
			killMeNow = true;
			GetComponent<Collider>().enabled = false;
        }
	}

	void OnCollisionEnter2D(Collision2D collision) {
		bool rescuedKitty = false;
		if(collision.transform.tag == "kitty") {
			Kitty k = collision.gameObject.GetComponent<Kitty>();
			rescuedKitty = k.rescued;
		}
		if (collision.transform.tag == "yarnBall" || rescuedKitty) {
			soundEffects.randomMagnet();
			SpawnRegister.spawn.MagnetAttack();	
			killMeNow = true;
			GetComponent<Collider2D>().enabled = false;
			YarnRegister.ball.ninjaTime = 0f;
		}
	}
}
