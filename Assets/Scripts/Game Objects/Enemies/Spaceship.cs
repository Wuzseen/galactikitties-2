using UnityEngine;
using System.Collections;

public class Spaceship : MonoBehaviour {
	public tk2dSprite spaceSprite;
	public bool debug;
	public Color crossHairColor;
	public GameObject laserShot;
	public Transform debugTarget;
	private tk2dSprite crosshair;
	public tk2dSprite laserRoot;
	private Spawners spawners;
	public Transform target; // Target is set by the spawner, in case we ever want to not target the ball for some reason
	private bool firing = false;
	private bool letTrack = true;
	private float entranceLength, fireLength, leaveLength;
	// Use this for initialization
	void Start () {
		spawners = Spawners.Instance;
		crosshair = this.transform.GetChild(0).GetComponent<tk2dSprite>();
		crosshair.color = crossHairColor;
		//crosshair.color.a = 0f;
		crosshair.transform.position = transform.position;
		if(debug) {
			target = debugTarget;
			Begin();
		}
	}
	
	public float Begin() {
		entranceLength = Random.Range(1f,3f);
		leaveLength = Random.Range(1f,3f);
		fireLength = Random.Range(4f,10f);//numberOfShots;
		StartCoroutine("EntranceSequence");
		return entranceLength + leaveLength + fireLength;
	}
	
	IEnumerator EntranceSequence() {
		this.transform.position = spawners.RandomSpawnSide().RandomPoint();
		Bounds bounds = spaceSprite.GetBounds();
		float width = bounds.extents.x/2f + 3f; // 3 = margin
		float height = bounds.extents.y/2f + 3f; // 3 = margin
		Vector3 spawnPoint = new Vector3();
		float leftX = Camera.main.ScreenToWorldPoint(Vector3.zero).x + width;
		float rightX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width,0,0)).x - width;
		spawnPoint.x = Random.Range(leftX,rightX);
		float topY = Camera.main.ScreenToWorldPoint(Vector3.zero).y + height;
		float bottomY = Camera.main.ScreenToWorldPoint(new Vector3(0,Screen.height,0)).y - height;
		spawnPoint.x = Random.Range(topY,bottomY);
		spawnPoint.z = 15;
		Go.to (this.transform, entranceLength, new GoTweenConfig().position(spawnPoint).setEaseType(GoEaseType.QuadOut));
		Go.to (this.spaceSprite, entranceLength, new GoTweenConfig().colorProp("color", new Color(255f,255f,255f,255f)));
		//int xPos = Random.Range (Camera.main.ScreenToWorldPoint(Vector3.;
		yield return new WaitForSeconds(entranceLength);
		firing = true;
		StartCoroutine("FireSequence");
		StartCoroutine("TrackSequence");
	}
	
	IEnumerator TrackSequence() {
		crosshair.transform.position = target.position;
		Color targetColor = new Color(crossHairColor.r,crossHairColor.g,crossHairColor.b,1);
		Go.to (crosshair,.7f,new GoTweenConfig().colorProp("color",targetColor,false).setEaseType(GoEaseType.Linear));
		float speed = Random.Range (2f,4f);
		while(true) {
			if(letTrack)
				crosshair.transform.position = Vector3.Lerp(crosshair.transform.position,target.position,speed * Time.deltaTime);
			yield return 0;
		}
	}
	
	void fireShot() {
		//print ("SHOTS FIRED AT " + target.position);
		if(soundEffects.Instance != null)
			soundEffects.Instance.PlayLaserShot ();
		float length = Random.Range(.5f,1f);
		GameObject laser = (GameObject)Instantiate(laserShot,laserRoot.transform.position,Quaternion.identity);
		//laser.transform.LookAt(crosshair.transform.position);
		float angle = Mathf.Atan2 (crosshair.transform.position.y,crosshair.transform.position.x) * Mathf.Rad2Deg;
		laser.transform.rotation = Quaternion.Euler(new Vector3(0,0,angle));
		Destroy (laser,length - .1f);
		Go.to (laser.transform, length, new GoTweenConfig().position(crosshair.transform.position).scale(1.3f,false).setEaseType(GoEaseType.ExpoInOut));
	}
	
	IEnumerator FireSequence() {
		int numberOfShots = Random.Range (2,7);
		fireLength /= numberOfShots;
		int shotsFired = 0;
		float chargeTime = .7f * fireLength;
		float coolDown = .3f * fireLength;
		Color laserRootC = new Color(laserRoot.color.r,laserRoot.color.g,laserRoot.color.b,0f);
		Color laserMaxAlphaRootC = new Color(laserRootC.r,laserRootC.g,laserRootC.b,1f);
		while(shotsFired < numberOfShots) {
			Go.to (laserRoot,chargeTime,new GoTweenConfig().colorProp("color",laserMaxAlphaRootC,false).setEaseType(GoEaseType.Linear));
			yield return new WaitForSeconds(chargeTime);
			letTrack = false;
			fireShot();
			Go.to (laserRoot,coolDown,new GoTweenConfig().colorProp("color",laserRootC,false).setEaseType(GoEaseType.Linear));
			yield return new WaitForSeconds(coolDown);
			letTrack = true;
			shotsFired++;
		}
		firing = false;
		Color targetColor = new Color(crossHairColor.r,crossHairColor.g,crossHairColor.b,0);
		Go.to (crosshair,.7f,new GoTweenConfig().colorProp("color",targetColor,false).setEaseType(GoEaseType.Linear));
		yield return new WaitForSeconds(.7f);
		StartCoroutine("LeaveSequence");
	}
	
	IEnumerator LeaveSequence() {
		Vector3 spawnPoint = spawners.RandomSpawnSide().RandomPoint();
		Go.to (this.transform, leaveLength, new GoTweenConfig().position(spawnPoint).setEaseType(GoEaseType.QuadIn));
		Go.to (this.spaceSprite, leaveLength, new GoTweenConfig().colorProp("color", new Color(255f,255f,255f,0f)));
		StopCoroutine("TrackSequence");
		yield return new WaitForSeconds(leaveLength);
	}
}
