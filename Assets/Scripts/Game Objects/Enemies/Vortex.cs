﻿using UnityEngine;
using System.Collections;

public class Vortex : MonoBehaviour {
	public float rotateSpeed;
	public SkeletonAnimation animate;
	public Gradient color;
	public AnimationCurve colorGradient;
	private Color currentColor;
	private float lifeTimer = 0f;
	public float timeAlive = 15f;
	// Use this for initialization
	void Start () {
		timeAlive += Random.Range(-3f,3f);
		Destroy (this.gameObject,timeAlive);
		SetColor(color.Evaluate(0f));
	}
	
	void SetColor(Color c) {
		animate.skeleton.r = c.r;
		animate.skeleton.g = c.g;
		animate.skeleton.b = c.b;
	}
	
	void Rotate () {
		transform.Rotate (0,0,rotateSpeed);
	}
	
	// Update is called once per frame
	void Update () {
		Rotate();
		float lifePercent = colorGradient.Evaluate(lifeTimer/timeAlive);
		SetColor(color.Evaluate(lifePercent)); // Uses curve
		lifeTimer += Time.deltaTime;
	}
	
	void OnTriggerEnter(Collider other) {
        if (other.tag != "yarnBall" && other.tag != "vortex" && other.name != "tractorCollider") {
			other.gameObject.SendMessage("Vortex",SendMessageOptions.DontRequireReceiver);
			Vector3 direction = other.transform.position - transform.position;
			direction = -direction.normalized;
			other.GetComponent<Rigidbody>().AddForce(direction * 5000f);
		}
	}
	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag != "yarnBall" && other.tag != "vortex" && other.name != "tractorCollider") {
			other.gameObject.SendMessage("Vortex",SendMessageOptions.DontRequireReceiver);
			Vector3 direction = other.transform.position - transform.position;
			direction = -direction.normalized;
			other.GetComponent<Rigidbody2D>().AddForce(direction * 5000f);
		}
	}
}
