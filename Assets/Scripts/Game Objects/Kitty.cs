#define k2dGalactikitties
using UnityEngine;
using System.Collections;


public class Kitty : MonoBehaviour {
	public bool bigKitty = false;
	public bool rescued = false;
	public SkeletonAnimation anim;
	private int c_;
	private bool autoPool = true;
	private bool fading = false;
	
	// Gets called when kitten is unpooled (when it's activated)
	// Turns physics back on and 'resets' it
	void Spawn () {
		#if k2dGalactikitties
		GetComponent<Rigidbody2D>().isKinematic = false;
		#else
		rigidbody2D.isKinematic = false;
		#endif
		autoPool = true;
		this.transform.parent = null;
		rescued = false;
		bigKitty = false;
		ColorMeKitty();
		StartCoroutine("PoolSoon");
	}
				
	Color GetColor() {
		return new Color(anim.skeleton.r,anim.skeleton.g,anim.skeleton.b);			
	}
	
	void SetColor(int c) {
		Dresser.SetSkeleColor(anim,c);
		c_ = c;
	}
	
	void ColorMeKitty() {
		int x = Random.Range(0,3);
		int c = 0;
		string skinName = "default";
		switch(x) {
		case 0:
			c = int.Parse(CatDataMono.data.cat1.colorIndex);
			skinName = CatDataMono.data.cat1.skinName;
			break;
		case 1:
			c = int.Parse(CatDataMono.data.cat2.colorIndex);
			skinName = CatDataMono.data.cat2.skinName;
			break;
		case 2:
			c = int.Parse(CatDataMono.data.cat3.colorIndex);
			skinName = CatDataMono.data.cat3.skinName;
			break;
		}
		anim.skeleton.SetSkin(skinName);
		anim.skeleton.SetSlotsToSetupPose();
		Dresser.SetSkeleColor(anim,c);
		c_ = c;
	}
	
	// Locks its rotation, the rigidbody lock seemed to not work when I made this, probably could/should get axed
	void Update() {
		if(transform.rotation.x > 0f || transform.rotation.x < 0f) {
			Quaternion temp = transform.rotation;
			temp.x = 0f;
			transform.rotation = temp;
		}
		if(transform.position.z != 10f) {
			Vector3 temp = transform.position;
			temp.z = 10f;
			transform.position = temp;
		}
	}
	
	IEnumerator PoolSoon() {
		float timeLimit = 15f;
		float elapsed = 0f;
		while(elapsed < timeLimit) {
			if(autoPool == false)
				return false;
			elapsed += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		PoolMe();
	}
	
	void PoolMe() {
		KittyPoolerRegister.Pooler.PoolKitten(this.gameObject);
	}
	
	IEnumerator fade() {
		fading = true;
		GetComponent<Rigidbody2D>().isKinematic = false;
		float timeLimit = .8f;
		float elapsed = 0f;
		float rotSpeed = Random.Range(-200f,200f);
		while(elapsed < timeLimit) {
			elapsed += Time.deltaTime;
			transform.localScale = transform.localScale * .98f;
			transform.Rotate(0, 0, rotSpeed * Time.deltaTime, Space.World);
			yield return new WaitForEndOfFrame();
		}
		GetComponent<Rigidbody2D>().isKinematic = true;
		PoolMe();
	}
	
	public void Vortex() {
		autoPool = false;
		#if k2dGalactikitties
		GetComponent<Collider2D>().enabled = false;
		#else
		collider.enabled = false;
		#endif
		YarnRegister.ball.collected.Remove(this.gameObject);
		if(rescued) {
			rescued = false;
			transform.parent = null;
		}
		if(!fading)
			StartCoroutine("fade");
	}
	
	public void Collect() {
		soundEffects.randomMeow ();
		autoPool = false;
		this.transform.parent = null;
		rescued = false;
		if(Application.loadedLevelName == "normal")
			Achievements.Instance.AddProgressToAchievement("Cat Collector",1 + (bigKitty ? 1 : 0));
		else if(Application.loadedLevelName == "timeAttack")
			Achievements.Instance.AddProgressToAchievement("Time Cat",1 + (bigKitty ? 1 : 0));
		else if(Application.loadedLevelName == "survival")
			Achievements.Instance.AddProgressToAchievement("Feline' Fine",1 + (bigKitty ? 1 : 0));
		Achievements.Instance.AddProgressToAchievement("Catamari",1 + (bigKitty ? 1 : 0));
		PoolMe();
	}
	
	public void Scatter() {
		#if k2dGalactikitties
		GetComponent<Collider2D>().enabled = false;
		#else
		collider.enabled = false;
		#endif
		autoPool = false;
		this.transform.parent = null;
		rescued = false;
		GetComponent<Rigidbody2D>().isKinematic = false;
		GetComponent<Rigidbody2D>().AddForce(10000f * new Vector3(Random.Range(0f,1f), Random.Range(0f,1f),0));
		transform.parent = null;
		if(!fading)
			StartCoroutine("fade");
		if(Application.loadedLevelName == "survival") {
			((SurvivalYarn)YarnRegister.ball).KittyHit();
		}
	}
	
	void OnCollisionEnter(Collision collision) {
		if(collision.transform.tag == "yarnBall" || (collision.transform.tag == "kitty" && collision.gameObject.GetComponent<Kitty>().rescued)) {
			if(!rescued) {
				soundEffects.randomStick();
				PulsePoolLoader.pooler.SpawnPulse(transform.position,c_);
				autoPool = false;
				transform.parent = collision.transform;
				//rigidbody2D.useGravity = false;
				GetComponent<Rigidbody2D>().isKinematic = true;
				rescued = true;
				YarnRegister.ball.collected.Add(this.gameObject);
			}
		}
	}

	void OnCollisionEnter2D(Collision2D collision) {
		if(collision.transform.tag == "yarnBall" || (collision.transform.tag == "kitty" && collision.gameObject.GetComponent<Kitty>().rescued)) {
			if(!rescued) {
				soundEffects.randomStick();
				PulsePoolLoader.pooler.SpawnPulse(transform.position,c_);
				autoPool = false;
				transform.parent = collision.transform;
				GetComponent<Rigidbody2D>().isKinematic = true;
				rescued = true;
				YarnRegister.ball.collected.Add(this.gameObject);
				YarnRegister.ball.ninjaTime = 0f;
			}
		}
	}
	
	void OnEnable() {
		#if k2dGalactikitties
		GetComponent<Collider2D>().enabled = true;
		#else
		collider.enabled = true;
		#endif
		Spawn ();
		fading = false;
		GetComponent<Renderer>().enabled = true;
		this.transform.localScale = ScaleConsts.catScale;
	}
	
	void OnDisable() {
		autoPool = false;
		GetComponent<Renderer>().enabled = false;
		#if k2dGalactikitties
		GetComponent<Collider2D>().enabled = false;
		#else
		collider.enabled = false;
		#endif
	}
}
