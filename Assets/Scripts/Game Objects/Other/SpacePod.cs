﻿using UnityEngine;
using System.Collections;

public class SpacePod : MonoBehaviour {
	public GameObject target, objectToSpawn;
	public AnimationCurve easingCurve;
	public static SpacePod activePod;
	public GameObject lid;
	private float timeLimit,timer;
	private Vector3 tarPos, startPos, offset;
	private bool allowToClick = true;
	public int axis;
	public bool debug;
//	private bool alive;
	// Use this for initialization
	
	void Start() {
		//alive = false;
		activePod = this;
		if(debug)
			SpacePodGo(null, 1);
	}
	
	public void Activate() {
		allowToClick = false;
		Debug.Log("I AM YOUR LEADER");
		activePod = null;
		StopCoroutine("oscillate");
		lid.GetComponent<Animation>().Play("openPod");
		Destroy (this.gameObject,3.0f);
		GameObject obj = (GameObject)Instantiate (objectToSpawn,transform.position,Quaternion.identity);
		if(objectToSpawn != null)
			obj.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 3000f);
	}
	
	void OpeningMoves () {
		//transform.positionTo(4f,new Vector3(10f,0f,0f),true);
		//alive = true;
		timer = 0f;
		timeLimit = 5f;
		tarPos = new Vector3(target.transform.position.x,target.transform.position.y,target.transform.position.z);
		startPos = new Vector3(transform.position.x,transform.position.y,transform.position.z);
		offset = Vector3.zero;
		StartCoroutine("Begin");
		StartCoroutine("oscillate");
	}
	
	public void SpacePodGo(GameObject objectToSpawn_, int direction) {
		objectToSpawn = objectToSpawn_;
		OpeningMoves();
	}
	
	IEnumerator oscillate () {
		float oscillationSpeed = .7f;
		float oscillationWidth = 30f;
		float oscillationTimer = 0f;
		int pingPong = 1;
		if(axis == 1) {// Y 
			while(true) {
				offset = new Vector3(0, easingCurve.Evaluate(oscillationTimer/oscillationSpeed) * oscillationWidth,0);
				oscillationTimer += Time.deltaTime * pingPong;
				if(oscillationTimer > oscillationSpeed || oscillationTimer < 0f)
					pingPong *= -1;
				yield return 0;
			}
		} else {
			while(true) {
				offset = new Vector3(easingCurve.Evaluate(oscillationTimer/oscillationSpeed) * oscillationWidth,0,0);
				oscillationTimer += Time.deltaTime * pingPong;
				if(oscillationTimer > oscillationSpeed || oscillationTimer < 0f)
					pingPong *= -1;
				yield return 0;
			}
		}
	}
	
	IEnumerator Begin() {
		while(true) {
			timer += Time.deltaTime;
			if( timer > timeLimit )
				Destroy(this.gameObject);
			transform.position = Vector3.Lerp(startPos,tarPos,timer/timeLimit);
			transform.position += offset;
			yield return 0;
		}
	}
	
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR || UNITY_STANDALONE
		bool inputTest = Input.GetKeyDown(KeyCode.Mouse0) && allowToClick;
		if(inputTest) {
			Debug.Log("I'm winning.");
			Vector3 p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			p.z = 10;
			if(Mathf.Abs(p.x - transform.position.x) < 10f && Mathf.Abs (p.y - transform.position.y) < 10f)
				Activate();
		}
#else
		bool inputTest = Input.touchCount >= 1;	
		if(inputTest) {
			Debug.Log("I'm winning.");
			Touch tch = Input.GetTouch(0);	
			Vector3 p = Camera.main.ScreenToWorldPoint(tch.position);
			p.z = 10;
			if(Mathf.Abs(p.x - transform.position.x) < 10f && Mathf.Abs (p.y - transform.position.y) < 10f)
				Activate();
		}
#endif
	}
}
