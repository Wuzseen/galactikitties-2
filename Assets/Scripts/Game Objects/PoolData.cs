﻿using UnityEngine;
using System.Collections;

public class PoolData {
	public Color c;
	public Vector3 t;
	// Use this for initialization
	public PoolData(Color c_, Vector3 t_) {
		c = c_;
		t = t_;
	}
}
