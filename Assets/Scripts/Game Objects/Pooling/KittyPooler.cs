﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KittyPooler : MonoBehaviour {
	private static KittyPooler inst;
	public GameObject[] kittenChoices;
	private List<GameObject> inactiveKittens;
	private List<GameObject> activeKittens;
	public int pooledKittens = 500; // Unchangeable for now, could this be dynamic?
	
	void Awake() {
		if(inst != null) {
			Destroy(this.gameObject);
			return;
		}
		inst = this;
		DontDestroyOnLoad(this);
		inactiveKittens = new List<GameObject>();
		activeKittens = new List<GameObject>();
		for(int i = 0; i < pooledKittens; i++ ) {
			inactiveKittens.Add((GameObject)Instantiate(kittenChoices[Random.Range(0,kittenChoices.Length)]));
			inactiveKittens[i].transform.parent = this.transform;
			inactiveKittens[i].SetActive(false);
		}
	}
	
	// This returns the current kitten and advances the kitten counter
	public GameObject GetKitten() {
		if(inactiveKittens.Count < 1)
			return null;
		GameObject kitty = inactiveKittens[0]; // Might want to use 0, could be faster.  Unsure at present though; I believe this will be faster as removeat is O(n)
		inactiveKittens.RemoveAt(0);
		
		activeKittens.Add(kitty);
		if(kitty != null)
			kitty.SetActive(true);
		return kitty;
	}
	
	public void PoolKitten(GameObject kitty) {
		activeKittens.Remove(kitty);
		inactiveKittens.Add(kitty);
		if(kitty != null && this != null) { // Hopefully not really necessary
			kitty.transform.position = this.transform.position;
			kitty.transform.parent = this.transform;
			kitty.SetActive(false);
		}
	}
	
	public void PoolAll() {
		while(activeKittens.Count > 0)
			PoolKitten(activeKittens[0]);
	}
}
