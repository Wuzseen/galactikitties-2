﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticlePooler : MonoBehaviour {
	public GameObject[] particlePrefabs;
	private List<GameObject> activePoofs;
	private List<GameObject> inactive;
	public int pooledPoofs;
	
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this);
		inactive = new List<GameObject>();
		activePoofs = new List<GameObject>();
		for(int i = 0; i < pooledPoofs; i++ ) {
			inactive.Add((GameObject)Instantiate(particlePrefabs[Random.Range(0,particlePrefabs.Length)]));
			inactive[i].transform.parent = this.transform;
			inactive[i].SetActive(false);
		}
	}
	
	public GameObject GetPoof() {
		if(inactive.Count < 1)
			return null;
		GameObject poof = inactive[0]; // Might want to use 0, could be faster.  Unsure at present though; I believe this will be faster as removeat is O(n)
		inactive.RemoveAt(0);
		activePoofs.Add(poof);
		if(poof != null)
			poof.SetActive(true);
		poof.transform.parent = null;
		return poof;
	}
	
	public void PoolPoof(GameObject poof) {
		activePoofs.Remove(poof);
		inactive.Add(poof);
		poof.transform.position = this.transform.position;
		poof.transform.parent = this.transform;
		poof.SetActive(false);
		//kitty.transform.localScale = ScaleConsts.catScale;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
