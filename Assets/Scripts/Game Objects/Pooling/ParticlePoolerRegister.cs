﻿using UnityEngine;
using System.Collections;

public class ParticlePoolerRegister : MonoBehaviour {

	public ParticlePooler linkPool;
	public static ParticlePooler Pooler;
	// Use this for initialization
	void Start () {
		if(Pooler != null) {
			Destroy(this.gameObject);
			return;
		}
		Pooler = linkPool;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
