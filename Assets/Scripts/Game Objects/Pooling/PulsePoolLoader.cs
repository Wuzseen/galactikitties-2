﻿using UnityEngine;
using System.Collections;

public class PulsePoolLoader : MonoBehaviour {
	public PulsePooler linkPooler;
	public static PulsePooler pooler;
	// Use this for initialization
	void Start () {
		if(pooler != null) {
			Destroy(this.gameObject);
			return;
		}
		pooler = linkPooler;
		pooler.Create();
		DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
