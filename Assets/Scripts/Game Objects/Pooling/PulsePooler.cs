﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PulsePooler : MonoBehaviour {
	public GameObject pulseObject;
	private List<GameObject> pulses;
	private List<GameObject> activePulses;

	// Use this for initialization
	public void Create () {
		pulses = new List<GameObject>();
		activePulses = new List<GameObject>();
		for(int i = 0; i < 100; i++) {
			pulses.Add((GameObject)Instantiate(pulseObject));	
			pulses[i].SetActive(false);
			pulses[i].transform.parent = transform;
		}
	}
	
	public void SpawnPulse(Vector3 t, int c) {
		GameObject ring = pulses[0];
		pulses.RemoveAt(0);
		activePulses.Add(ring);
		ring.transform.parent = null;
		ring.transform.localScale = new Vector3(1f,1f,1f);
		ring.SetActive(true);
		ring.SendMessage("Pulse",new PoolData(CatDataMono.catColors[c],t));
	}
	
	public void PoolPulse(GameObject pulse) {
		pulse.transform.parent = transform;
		pulses.Add(pulse);
		pulse.SetActive(false);
	}
}
