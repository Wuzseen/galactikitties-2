using UnityEngine;
using System.Collections;

public class SpawnAnimator : MonoBehaviour {
	//private float speed = marginf;
	//private bool go = true;
    public Camera theCam;
	public bool yAxis = true; // False means it's an xAxis object
	public int reverse = 1; // -1 will reverse movement
	public bool far = true; // The position, if it's yAxis, far means the bottom spawner.  If it's xAxis far means the right spawner
	// Use this for initialization
	public Vector3 startPosition, endPosition;
	void Start () {
		DontDestroyOnLoad(this.gameObject);
		Camera camera = theCam;
		int margin = 15;
		if(yAxis && !far) { // Left spawner
			startPosition = camera.ScreenToWorldPoint(new Vector3(0,0,camera.nearClipPlane));
			endPosition = camera.ScreenToWorldPoint(new Vector3(0,Screen.height,camera.nearClipPlane));
			startPosition.x = startPosition.x - margin;
			endPosition.x = endPosition.x - margin;
		} else if(yAxis && far) { // Right spawner
			startPosition = camera.ScreenToWorldPoint(new Vector3(Screen.width,0,camera.nearClipPlane));
			endPosition = camera.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,camera.nearClipPlane));
			startPosition.x = startPosition.x + margin;
			endPosition.x = endPosition.x + margin;
		} else if(!yAxis && !far) { // Top Spawner
			startPosition = camera.ScreenToWorldPoint(new Vector3(0,0,camera.nearClipPlane));
			endPosition = camera.ScreenToWorldPoint(new Vector3(Screen.width,0,camera.nearClipPlane));
			startPosition.y = startPosition.y - margin;
			endPosition.y = endPosition.y - margin;
		} else { // Bottom spawner
			startPosition = camera.ScreenToWorldPoint(new Vector3(0,Screen.height,camera.nearClipPlane));
			endPosition = camera.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,camera.nearClipPlane));
			startPosition.y = startPosition.y + margin;
			endPosition.y = endPosition.y + margin;
		}
		
		if(reverse == -1) {
			Vector3 temp = startPosition;
			startPosition = endPosition;
			endPosition = temp;
		}
		startPosition.z = 10f;
		endPosition.z = 10f;
		transform.position = startPosition;
	}
	
	// Update is called once per frame
	void Update () {	 
		transform.position = Vector3.Lerp(startPosition,endPosition, Mathf.PingPong(Time.time, 1.0f));
		/*if(transform.position.magnitude == endPosition.magnitude) {
			Transform temp = endPosition;
			endPosition = startPosition;
			startPosition = temp;
		}*/
	}
}
