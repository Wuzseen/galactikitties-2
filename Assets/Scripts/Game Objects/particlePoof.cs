using UnityEngine;
using System.Collections;

public class particlePoof : MonoBehaviour {
	// Use this for initialization
	void Start () {
	}
	
	
	
	// Update is called once per frame
	void Update () {
	
	}
	
	IEnumerator PoofTime() {
		GetComponent<ParticleSystem>().Play();
		float timeLimit = 3f;
		float time = 0f;
		while(time < timeLimit) {
			time += Time.deltaTime;
			yield return 0;
		}
		ParticlePoolerRegister.Pooler.PoolPoof(this.gameObject);
	}
	
	void OnEnable() {
		StartCoroutine("PoofTime");
	}
}
