﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Single action class.
public abstract class GameAction {
	protected List<AudioClip> audioList;
	public List<AudioClip> AudioList {
		get { return this.audioList; }
		set { audioList = value; }
	}

	protected List<GameAction> subActions;
	public List<GameAction> SubActions {
		get { return this.subActions; }
		set { subActions = value; }
	}


	public GameAction() {
		this.audioList = new List<AudioClip>();
		this.subActions = new List<GameAction>();
	}

	protected AudioClip randomAudioClip() {
		return audioList.Count > 0 ? audioList[Random.Range(0,audioList.Count)] : null;
	}

	public void AddClip(AudioClip clip) {
		audioList.Add(clip);
	}

	public bool RemoveClip(AudioClip clip) {
		return audioList.Remove(clip);
	}

	public void RemoveClipAt(int index) {
		audioList.RemoveAt(index);
	}

	protected abstract void ActionBehavior();

	// Resolve action with random, single clip
	protected virtual void ResolveAction() {
		this.ActionBehavior();
		foreach(GameAction subAction in subActions) {
			subAction.ResolveAction();
		}
	}

	public virtual void ResolveActionRandomAudio() {
		this.ResolveAction();
		this.randomAudioClip();
	}
}
