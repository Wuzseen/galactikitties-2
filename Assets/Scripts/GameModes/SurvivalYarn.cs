﻿using UnityEngine;
using System.Collections;

public class SurvivalYarn : YarnBall {
	public int MaxKittyHealth;
	private int currentHP;
	public int HP {
		get {
			return currentHP;
		}
	}
	public UISprite lifeSprite;
	public TweenColor tween;
	public UILabel timeSurvivedLabel;
    private bool endLevelNow = false;
	// Use this for initialization
	new void Start () {
		base.Start();
		
		if(GoogleAnalytics.instance) {
			GoogleAnalytics.instance.LogScreen("Survival Mode");
		}
		goTimer = 0f;
		if(Difficulty.ActiveDifficulty == DifficultyLevel.Hard) {
			MaxKittyHealth = (int)(.6f * (float)MaxKittyHealth);
		} else if(Difficulty.ActiveDifficulty == DifficultyLevel.Medium) {
			MaxKittyHealth = (int)(.8f * (float)MaxKittyHealth);
		}
		currentHP = MaxKittyHealth;
        winner = true;
		LevelReg.lvlObj.setMode(2); // Set to classic mode
		StartCoroutine("AchievementTimer");
	}

	void UpdateLifeSprite() {
		lifeSprite.fillAmount = ((float)currentHP)/((float)MaxKittyHealth);
		print ("Updatorino");
	}

	public void KittyHit() {
		currentHP--;
		UpdateLifeSprite();
		if(lifeSprite.fillAmount <= .35f) {
			tween.enabled = true;
			tween.Play();
			Warning.Alert("Watch Out!");
		}
		if(currentHP <= 0) {
			levelEnding = true;
			EndLevel();
		}
	}

	public void HPPlus(int amount) {
		currentHP += amount;
		if(currentHP > MaxKittyHealth) {
			currentHP = MaxKittyHealth;
		}
		UpdateLifeSprite();
	}

	IEnumerator AchievementTimer() {
		while(true) {
			if(Time.timeSinceLevelLoad > 60) {
				Achievements.Instance.AddProgressToAchievement("Nine Lives",1);
				break;
			}
			yield return 0;
		}
	}

    public void Lose() {
		if(endLevelNow)
			return;
        lifeSprite.GetComponent<Animation>().Play("fadeOut");
		Warning.Alert("Game Over!",3f);
        Destroy(lifeSprite.gameObject, 2f);
		if(timer> PlayerPrefs.GetInt ("SurvivalTime")){
			int setTime = (int) timer;
			PlayerPrefs.SetInt ("SurvivalTime", setTime);
		}
        winner = false;
        endLevelNow = true;
    }
	
	// Update is called once per frame
    void Update()
    {
        if (!go)
        {
            if (!paused)
                goTimer += Time.deltaTime;
            if (goTimer > 3f)
            {
                goTimer = 0;
                go = true;
            }
        }
        else if (!paused)
        {
            Time.timeScale = 1f;
            if (levelEnding == false)
                timer += Time.deltaTime;

            if (!levelEnding && ((neededKittens - collectedKittens <= 0)  || endLevelNow))
            {
                levelEnding = true;
                EndLevel();
            }

            if (collected.Count <= 0)
                collectTimer = 0f;
            if (tractorDetector.inBeam && collectTimer <= .2f && !levelEnding)
            {
                // If in beam, increment kitty collector timer by delta time.  If timer > .2 seconds, collect the most recently collected kitten
                collectTimer += Time.deltaTime;
                outTimer = .5f;
            }
            else if (tractorDetector.inBeam && collectTimer > .2f && !levelEnding)
            {
                kittyCombo++;
                for (int i = 0; i < kittyCombo; i++)
                    collectKitty();
            }
            else if (!tractorDetector.inBeam && outTimer > 0f && !levelEnding)
            {
                outTimer -= Time.deltaTime;
            }
            else if (!levelEnding)
            {
                collectTimer = 0f;
                outTimer = 0f;
                kittyCombo = 1;
            }
        }
        else
        {
            Time.timeScale = 0f;
        }

        if (!paused)
        {
            Rotate();
            HandleInput(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            paused = !paused;
        GUIStuff();
    }

    new void collectKitty()
    {
        if (collected.Count > 0)
        {
            if (neededKittens >= collectedKittens)
                togoLabel.GetComponent<Animation>().CrossFade("kittyCollect");
            collectedKittens++;
            collectTimer = 0f;
            outTimer = .5f;
            GameObject theKitty = collected[collected.Count - 1];
            Kitty kittyComp = theKitty.GetComponent<Kitty>();
            if (theKitty != null && kittyComp.bigKitty)
                bigKitty = 2;
            else
                bigKitty = 1;
            collected.RemoveAt(collected.Count - 1);
            if (theKitty != null)
            {
                ParticlePoolerRegister.Pooler.GetPoof().transform.position = theKitty.transform.position;
                kittyComp.Collect();
            }

            score += 1 * bigKitty;
        }
    }

    new void GUIStuff()
    {
		timeSurvivedLabel.text = string.Format("Time Survived: {0:D}", (int)Time.timeSinceLevelLoad);
        scoreLabel.text = "Score: " + score.ToString();
        string togoString = neededKittens - collectedKittens < 0 ? "0" : (neededKittens - collectedKittens).ToString();
        togoLabel.text = "To Go: " + togoString.ToString();
		levelLabel.text = "Level " + LevelReg.lvlObj.getLevel();
		catnipTimer.fillAmount = catnip/maxCatnip;// = new Rect(-1f + catnip/maxCatnip,0f,1f,1f);
//        if (catnip > 0)
//            catnipLabel.enabled = true;
//        else
//            catnipLabel.enabled = false;
		
		if(!go && paused) {
			getReady.GetComponent<Animation>().Stop();
			getReady.enabled = false;
		}
		else if(!go) {
			getReady.enabled = true;
			getReady.GetComponent<Animation>().Play("getReady");
		}
		else  {
			getReady.GetComponent<Animation>().Stop();
			getReady.enabled = false;
		}
        if (paused)
        {
            foreach (GameObject pauser in pauseObjects)
                pauser.SetActive(true);
        }
        else
        {
            foreach (GameObject pauser in pauseObjects)
                pauser.SetActive(false);
        }
    }
}
