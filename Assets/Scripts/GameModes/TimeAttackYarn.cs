using UnityEngine;
using System.Collections;

public class TimeAttackYarn : YarnBall {
	new void Start() {
		
		if(GoogleAnalytics.instance) {
			GoogleAnalytics.instance.LogScreen("Time Attack Mode");
		}
		goTimer = 0f;
		highScore = PlayerPrefs.GetInt("TimeCats");
		LevelReg.lvlObj.setMode(1); // Set to classic mode
		NewLevel();
	}
	
	// Update is called once per frame
	void Update () {
		if(!go) {
			if( !paused)
				goTimer += Time.deltaTime;
			if( goTimer > 3f) {
				goTimer = 0;
				go = true;
			}
		} else if(!paused) {
			Time.timeScale = 1f;
			if(levelEnding == false) {
				timer += Time.deltaTime;
				collectedKittens = collected.Count;
			}
			if(timer > gameLimit) {
				winner = false;
				levelEnding = true;
				EndLevel();
			} else if (neededKittens - collectedKittens <= 0 && !levelEnding) {
				winner = true;
				levelEnding = true;
				EndLevel();
			}
			
			collectedKittens = collected.Count;
			
		} else {
			Time.timeScale = 0f;	
		}
		if(!paused) {
			Rotate ();
			HandleInput(Camera.main.ScreenToWorldPoint(Input.mousePosition));
		}
			
		
		if(Input.GetKeyDown(KeyCode.Escape))
			paused = !paused;
		if(score > highScore)
			PlayerPrefs.SetInt("TimeScore", score);
		GUIStuff();
	}
		
	new void NewLevel() {
		winner = false;
		TimeLabelShrink();
		timeLabel.color = Color.white;
		levelEnding = false;
		collectTimer = 0;
		neededKittens = LevelReg.lvlObj.survivalKittens();
		timer = 0;
		gameLimit = LevelReg.lvlObj.survivalTime();
		collectedKittens = 0;
		paused = false;
		flip = LevelReg.lvlObj.getLevel() % 2 == 0 ? -1f : 1f;
		kittyCombo = 1;
		collectedKittens = 0;
	}
	
	new void collectKitty() {
		totalKittensThisGame++;
		if(collected.Count > 0) {
			if(neededKittens >= collectedKittens)
				togoLabel.GetComponent<Animation>().CrossFade("kittyCollect");
			collectTimer = 0f;
			outTimer = .5f;
			GameObject theKitty = collected[collected.Count - 1];
			Kitty kittyComp = theKitty.GetComponent<Kitty>();
			if(theKitty != null && kittyComp.bigKitty)
				bigKitty = 2;
			else
				bigKitty = 1;
			collected.RemoveAt (collected.Count - 1);
			if(theKitty != null) {
				ParticlePoolerRegister.Pooler.GetPoof().transform.position = theKitty.transform.position;
				kittyComp.Collect();
			}
			score += 1 * bigKitty;
		}
	}
	
	IEnumerator EndLevelCoRoutine() {
		TimeLabelScoring();
		timeLabel.text = "Scoring Points!";
		float breakTimer = 0f; // Mininum break at the end of a level, like if they just beat the level without the bonus round
		collectTimer = 0f;
		kittyCombo = 1;
		float totalWait = 2f;
		float partialWait = totalWait / (float)collected.Count;
		while(collected.Count > 0 ) {
			collectTimer += Time.deltaTime;
//			for(int i = 0; i < kittyCombo; i++) {
//				if(collectTimer >= .1f && collected.Count > 0) {
					collectKitty();
//				}
//			}
			yield return new WaitForSeconds(partialWait); // Wait a frame
		}
		
		// Logic below seems...... bad, should clean it up.
		
		LevelReg.lvlObj.addScore(score);
		if(winner) {
			timeLabel.text = "New Level!";
			soundEffects.newLevel();
		} else {
			timeLabel.text = "GAME OVER";
		}
		
		while(breakTimer < 2f) {
			breakTimer += Time.deltaTime;
			yield return 0;
		}
		
		if(winner) {
			LevelReg.lvlObj.newLevel();
			NewLevel();
		} else{
			if(totalKittensThisGame > PlayerPrefs.GetInt ("TimeCats"))
				PlayerPrefs.SetInt("TimeCats", PlayerPrefs.GetInt ("TimeCats") + totalKittensThisGame);
			Application.LoadLevel("highScore");
		}
	}
	
	new void GUIStuff() {
		if(timer < gameLimit - 5f && timer > gameLimit - 5f - .2f ) { // five seconds left
			timeLabel.transform.scaleTo(.2f,Vector3.one * 1.4f);
			timeLabel.transform.localPositionTo(.3f,new Vector3(0f,-warningOffset),true);
			timeTextGrown= true;
		}
		else if(timer > gameLimit - 5f && !levelEnding) {
			timeLabel.color = Color.red;
		}
		scoreLabel.text = "Score: " + score.ToString();
		string togoString = neededKittens - collectedKittens < 0 ? "0" : (neededKittens - collectedKittens).ToString();
		togoLabel.text = "To Go: " + togoString.ToString();
		if(!levelEnding) {
			timeLabel.text = timeString();
		}
		levelLabel.text = "Level " + LevelReg.lvlObj.getLevel();
		catnipTimer.fillAmount = catnip/maxCatnip;// = new Rect(-1f + catnip/maxCatnip,0f,1f,1f);
//		if(catnip > 0)
//			catnipLabel.enabled = true;
//		else
//			catnipLabel.enabled = false;
		
		if(!go && paused) {
			getReady.GetComponent<Animation>().Stop();
			getReady.enabled = false;
		}
		else if(!go) {
			getReady.enabled = true;
			getReady.GetComponent<Animation>().Play("getReady");
		}
		else  {
			getReady.GetComponent<Animation>().Stop();
			getReady.enabled = false;
		}
		if(paused) {
			foreach(GameObject pauser in pauseObjects)
				pauser.SetActive(true);
		}
		else {
			foreach(GameObject pauser in pauseObjects)
				pauser.SetActive(false);
		}
	}
}
