﻿using UnityEngine;
using System.Collections;

public class TutorialYarn : YarnBall {
	public bool waitOnTutorial = false;
	
	// Update is called once per frame
	void Update () {
		if(!paused) {
			Time.timeScale = 1f;
			if(levelEnding == false && !waitOnTutorial)
				timer += Time.deltaTime;

			if((timer > gameLimit))
				winner = false;
			else if(neededKittens - collectedKittens <= 0)
				winner = true;
			if((timer > gameLimit || (neededKittens - collectedKittens) <= 0) && !levelEnding ) {
				levelEnding = true;
				EndLevel();
			}
			
			if(collected.Count <= 0)
				collectTimer = 0f;
			if(tractorDetector.inBeam && collectTimer <= .2f && !levelEnding) {
				// If in beam, increment kitty collector timer by delta time.  If timer > .2 seconds, collect the most recently collected kitten
				collectTimer += Time.deltaTime;	
				outTimer = .5f;
			}
			else if(tractorDetector.inBeam && collectTimer > .2f && !levelEnding) {
				kittyCombo++;
				for(int i = 0; i < kittyCombo; i++)
					collectKitty();
			} else if(!tractorDetector.inBeam && outTimer > 0f && !levelEnding) {
				outTimer -= Time.deltaTime;
			} else if(!levelEnding) {
				collectTimer = 0f;
				outTimer = 0f;
				kittyCombo = 1;
			}
		} else if(paused) {
			Time.timeScale = 0f;	
		}
		
		if(!paused) {
			Rotate ();
			HandleInput(Camera.main.ScreenToWorldPoint(Input.mousePosition));
		}
		
		if(Input.GetKeyDown(KeyCode.Escape))
			paused = !paused;
		if(score > highScore)
			PlayerPrefs.SetInt("classicScore", score);
		GUIStuff();
	}
	
	new void GUIStuff() {
		if(timer < gameLimit - 5f && timer > gameLimit - 5f - timeLabel.GetComponent<Animation>()["grow"].length ) {
			TimeLabelWarn();
			timeTextGrown= true;
		}
		else if(timer > gameLimit - 5f && !levelEnding) {
			timeLabel.color = Color.red;
		}
		scoreLabel.text = "Score: " + score.ToString();
		string togoString = neededKittens - collectedKittens < 0 ? "0" : (neededKittens - collectedKittens).ToString();
		togoLabel.text = "To Go: " + togoString.ToString();
		if(!levelEnding) {
			timeLabel.text = timeString();
		}
		levelLabel.text = "Tutorial";
		catnipTimer.fillAmount = catnip/maxCatnip;// = new Rect(-1f + catnip/maxCatnip,0f,1f,1f);
//		if(catnip > 0)
//			catnipLabel.enabled = true;
//		else
//			catnipLabel.enabled = false;
		
		if(paused) {
			foreach(GameObject pauser in pauseObjects)
				pauser.SetActive(true);
		}
		else {
			foreach(GameObject pauser in pauseObjects)
				pauser.SetActive(false);
		}
	}
}
