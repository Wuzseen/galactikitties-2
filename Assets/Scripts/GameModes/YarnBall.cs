using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class YarnBall : MonoBehaviour {
	public UILabel scoreLabel, togoLabel, timeLabel, getReady, levelLabel;
	public Image catnipTimer;
	public GameObject[] pauseObjects;
	public KittySpawner theSpawner;
	public float catNipForce = 4.5f;
	protected float collectTimer, goTimer;
	public List<GameObject> collected = new List<GameObject>();
	protected float gameLimit, timer, catnip, flip;
	protected int score;
	protected int neededKittens, collectedKittens;
	public bool paused;
	public bool go = false;
	public DetectTractorBeam tractorDetector;
	public TractorBeamSpawner tractorSpawner;
	protected bool timeTextGrown = false;
	protected bool winner = false;
	protected int kittyCombo = 1;
	protected float outTimer = 0f;
	protected int bigKitty = 1;
	public bool levelEnding = false;
	public int totalKittensThisGame;
	public int highScore;
	public float maxCatnip = 7f;
	public float ninjaTime = 0f;

	protected float warningOffset = 80f;

	protected Vector3 timeLabelStartPosition = new Vector3(0f,-40f,0f);
	protected Vector3 timeLabelStartScale = Vector3.one;
	protected Vector3 timeLabelOffsetPosition = new Vector3(0f,-250f,0f);

	void Awake () {
		if(GoogleAnalytics.instance) {
			GoogleAnalytics.instance.LogScreen("Normal Mode");
		}
		goTimer = 0f;
		goTimer = 0f;
		highScore = PlayerPrefs.GetInt("ClassicScore");
		LevelReg.lvlObj.setMode(0); // Set to classic mode
		NewLevel();
		StartCoroutine("NinjaTimer");
	}

	protected void TimeLabelShrink() {	
		timeLabel.transform.scaleTo(.3f,timeLabelStartScale);
		timeLabel.transform.localPositionTo(.3f,timeLabelStartPosition);
	}
	
	protected void TimeLabelWarn() {	
		timeLabel.transform.scaleTo(.3f,timeLabelStartScale * 1.25f);
		timeLabel.transform.localPositionTo(.3f, timeLabelOffsetPosition);
	}

	protected void TimeLabelScoring() {
		timeLabel.transform.scaleTo(.3f,timeLabelStartScale * 1.5f);
		timeLabel.transform.localPositionTo(.3f, timeLabelOffsetPosition);
	}

	IEnumerator NinjaTimer() {
		if(Application.loadedLevelName == "tutorial") {
			yield return false;
		} else {
			print ("Ninja go");
			while(ninjaTime <= 60f) {
				ninjaTime += Time.deltaTime;
				yield return 0;
			}
			Achievements.Instance.AddProgressToAchievement("Neeeenja",1);
		}
	}

	protected void Start () {
	}
	
	public int totalCollected() {
		return collected.Count;
	}
	
	public void addCatnip() {
		catnip = catnip + 3f;
		if(catnip > maxCatnip)
			catnip = maxCatnip;
	}
	
	protected void Rotate() {
		if(catnip > 0) {
			catnip -= Time.deltaTime;
			theSpawner.spawnTime = .5f;
		}
		else {
			catnip = 0;
			theSpawner.spawnTime = 1f;
		}
		float catNipMod = catnip > 0 ? catNipForce : 1;
		float rotateSpeed = flip * (catNipMod) * .1f * (collected.Count + 1);
		if(rotateSpeed > 1f * catNipMod)
			rotateSpeed = 1f * catNipMod;
		transform.Rotate(0,0,rotateSpeed);
	}
	
	protected void EndLevel() {
		Achievements.Instance.SaveAchievements();
		StartCoroutine("EndLevelCoRoutine");
	}
	
	// Reset the level
	IEnumerator EndLevelCoRoutine() {
        if (timeLabel != null) {
			TimeLabelScoring();
			timeLabel.text = "Bonus Saves!";
			timeTextGrown = false;
        }
		float breakTimer = 0f; // Mininum break at the end of a level, like if they just beat the level without the bonus round
		tractorSpawner.Reset();
		tractorDetector.enabled = false;
		collectTimer = 0f;
		kittyCombo = 1;
		float totalWait = 2f;
		float partialWait = totalWait / (float)collected.Count;
		while(collected.Count > 0 ) {
			collectTimer += partialWait;
//			for(int i = 0; i < kittyCombo; i++) {
//				if(collectTimer >= .1f && collected.Count > 0) {
					collectKitty();
//				}
//			}
			yield return new WaitForSeconds(partialWait); // Wait a frame
		}
		if(winner) {
            if(timeLabel != null) {
			    timeLabel.text = "New Level!";
			}
			soundEffects.newLevel();
		}
		else if(timeLabel != null) {
			if(collectedKittens < neededKittens) {
				timeLabel.text = "Game Over!";
			} else {
				timeLabel.text = "New Level!";
			}
		}
		
		while(breakTimer < 2f) {
			breakTimer += Time.deltaTime;
			yield return 0;
		}
		
		LevelReg.lvlObj.addScore(score);
		if(neededKittens - collectedKittens <= 0) {
			LevelReg.lvlObj.newLevel();
			NewLevel();
		} else{
			if(totalKittensThisGame > PlayerPrefs.GetInt ("ClassicCats"))
				PlayerPrefs.SetInt("ClassicCats", PlayerPrefs.GetInt ("ClassicCats") + totalKittensThisGame);
			SoundController.PlayNextSong();
			Application.LoadLevel("highScore");
		}
	}
	
	protected void NewLevel() {
        if (timeLabel != null) { 
			TimeLabelShrink();
		    timeLabel.color = Color.white;
        }
		if(tractorSpawner != null) // Not always a tractor spawner in a game.
			tractorSpawner.Go();
		levelEnding = false;
		collectTimer = 0;
		neededKittens = LevelReg.lvlObj.requiredKittens();
		//score = LevelReg.lvlObj.getScore();
		timer = 0;
		gameLimit = LevelReg.lvlObj.timeLimit();
		collectedKittens = 0;
		paused = false;
		flip = LevelReg.lvlObj.getLevel() % 2 == 0 ? -1f : 1f;
		if(tractorDetector != null)
			tractorDetector.enabled = true;
		kittyCombo = 1;
		collectedKittens = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(!go) {
			if( !paused)
				goTimer += Time.deltaTime;
			if( goTimer > 3f) {
				goTimer = 0;
				go = true;
			}
		} else if(!paused) {
			Time.timeScale = 1f;
			if(levelEnding == false)
				timer += Time.deltaTime;
			
			if((timer > gameLimit))
				winner = false;
			else if(neededKittens - collectedKittens <= 0)
				winner = true;
			if((timer > gameLimit || (neededKittens - collectedKittens) <= 0) && !levelEnding ) {
				levelEnding = true;
				EndLevel();
			}
			
			if(collected.Count <= 0)
				collectTimer = 0f;
			if(tractorDetector.inBeam && collectTimer <= .2f && !levelEnding) {
				// If in beam, increment kitty collector timer by delta time.  If timer > .2 seconds, collect the most recently collected kitten
				collectTimer += Time.deltaTime;	
				outTimer = .5f;
			}
			else if(tractorDetector.inBeam && collectTimer > .2f && !levelEnding) {
				kittyCombo++;
				for(int i = 0; i < kittyCombo; i++)
					collectKitty();
			} else if(!tractorDetector.inBeam && outTimer > 0f && !levelEnding) {
				outTimer -= Time.deltaTime;
			} else if(!levelEnding) {
				collectTimer = 0f;
				outTimer = 0f;
				kittyCombo = 1;
			}
		} else {
			Time.timeScale = 0f;	
		}
		
		if(!paused) {
			Rotate ();
			HandleInput(Camera.main.ScreenToWorldPoint(Input.mousePosition));
		}
		
		if(Input.GetKeyDown(KeyCode.Escape))
			paused = !paused;
		if(score > highScore)
			PlayerPrefs.SetInt("classicScore", score);
		GUIStuff();
	}
	
	protected void HandleInput(Vector3 newPos) {
		newPos.z = 10;
		transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * 5.0f);
	}
	
	protected void collectKitty() {
		if(collected.Count > 0) {
			totalKittensThisGame ++;
			if(neededKittens >= collectedKittens)
				togoLabel.GetComponent<Animation>().CrossFade("kittyCollect");
			collectedKittens++;
			collectTimer = 0f;
			outTimer = .5f;
			GameObject theKitty = collected[collected.Count - 1];
			Kitty kittyComp = theKitty.GetComponent<Kitty>();
			if(theKitty != null && kittyComp.bigKitty)
				bigKitty = 2;
			else
				bigKitty = 1;
			collected.RemoveAt (collected.Count - 1);
			if(theKitty != null) {
				ParticlePoolerRegister.Pooler.GetPoof().transform.position = theKitty.transform.position;
				kittyComp.Collect();
			}

			score += 1 * bigKitty;
		}
	}
	
	public void ScatterTheKittens(Transform theKitten) {
		if(theKitten.childCount < 1) {
			collected.Remove(theKitten.gameObject);
			theKitten.GetComponent<Kitty>().Scatter();
			return;
		}
		else {
			foreach(Transform kitten in theKitten) {
				ScatterTheKittens(kitten);
				collected.Remove(theKitten.gameObject);
				theKitten.GetComponent<Kitty>().Scatter();
			}
		}
		return;
	}
	
	public void addKitty(GameObject theKitty) {
		collected.Add(theKitty);	
	}
	
	protected string timeString() {
		float elapsed = gameLimit - timer;
		
		return elapsed.ToString("0000");
	}
	
	protected void GUIStuff() {
		if(timer < gameLimit - 5f && timer > gameLimit - 5f - timeLabel.GetComponent<Animation>()["grow"].length ) {
			TimeLabelWarn();
			timeTextGrown= true;
		}
		else if(timer > gameLimit - 5f && !levelEnding) {
			timeLabel.color = Color.red;
		}
		scoreLabel.text = "Score: " + score.ToString();
		string togoString = neededKittens - collectedKittens < 0 ? "0" : (neededKittens - collectedKittens).ToString();
		togoLabel.text = "To Go: " + togoString.ToString();
		if(!levelEnding) {
			timeLabel.text = timeString();
		}
		levelLabel.text = "Level " + LevelReg.lvlObj.getLevel();
		catnipTimer.fillAmount = catnip/maxCatnip;
//		if(catnip > 0)
//			catnipLabel.enabled = true;
//		else
//			catnipLabel.enabled = false;
		if(!go && paused) {
			getReady.GetComponent<Animation>().Stop();
			getReady.enabled = false;
		}
		else if(!go) {
			getReady.enabled = true;
			getReady.GetComponent<Animation>().Play("getReady");
		}
		else  {
			getReady.GetComponent<Animation>().Stop();
			getReady.enabled = false;
		}
		if(paused) {
			foreach(GameObject pauser in pauseObjects)
				pauser.SetActive(true);
		}
		else {
			foreach(GameObject pauser in pauseObjects)
				pauser.SetActive(false);
		}
	}
	
	void OnDestroy() {
		paused = false;
		Time.timeScale = 1f;
	}
}
