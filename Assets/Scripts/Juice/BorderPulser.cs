﻿using UnityEngine;
using System.Collections;

public class BorderPulser : MonoBehaviour {
	public static BorderPulser border;
	public UISprite sprite;
	public Color bad, good;
	private float startAlpha;
	private Color startColor, currentColor;
	// Use this for initialization
	void Start () {
		startColor = sprite.color;
		border = this;
	}
	
	public void Bad() {
		sprite.color = bad;
	}
	
	public void Good() {
		sprite.color = good;
	}
	
	// Update is called once per frame
	void Update () {
		sprite.color = Color.Lerp(sprite.color,startColor,Time.deltaTime);
	}
}
