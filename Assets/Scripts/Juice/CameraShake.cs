﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {
	
	private Vector3 originPosition;
	private float shake_decay, shake_intensity;
	
	// Use this for initialization
	void Start () {
		originPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if(shake_intensity > 0) {
			transform.position = originPosition + Random.insideUnitSphere * shake_intensity;
			shake_intensity -= shake_decay;
		}
	}
	
	public void Shake (float intensity, float decay) {
		shake_intensity = intensity;
		shake_decay = decay;
	}
}
