﻿using UnityEngine;
using System.Collections;

public class FingerFollower : MonoBehaviour {
	public Transform lineBase;
	public LineRenderer fingerLine;
	public tk2dSprite fingerSprite;
	private Vector3 lineEnd;
	private Color alphaedC, baseC, currentC;
	// Use this for initialization
	void Start () {
		alphaedC = fingerSprite.color;
		baseC = alphaedC;
		currentC = baseC;
		baseC.a = 1f;
	}
	
	void DecreaseAlpha() {
		currentC = Color.Lerp(currentC,alphaedC,3*Time.deltaTime);
		fingerSprite.color = currentC;
		fingerLine.SetColors(currentC,currentC);
	}
	
	void FixedUpdate() {
		Vector3 newB = lineBase.position;
		newB.z = 12;
		fingerLine.SetPosition(0,newB);
		if(Input.GetMouseButton(0)) {
			currentC = baseC;
			Vector3 finger = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			finger.z = 12;
			transform.position = finger;
			fingerLine.SetPosition(1,finger);
		}
		DecreaseAlpha();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
