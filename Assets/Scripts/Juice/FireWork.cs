﻿using UnityEngine;
using System.Collections;

public class FireWork : MonoBehaviour {
	public float speed = 1f;
	
	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody>().AddForce(Vector3.up * speed);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
