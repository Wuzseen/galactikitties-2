﻿using UnityEngine;
using System.Collections;

public class MainTitleAttract : MonoBehaviour {
    public UISprite title;
    public GoEaseType ease;
    public Color[] colors;

	// Use this for initialization
	void Start () {
        StartCoroutine("ColorizeMe");
        StartCoroutine("BulgeMe");
	}

    IEnumerator BulgeMe() {
        // It's a battle for the bulge #easteregg#420noscope
        yield return new WaitForSeconds(6f);
        while (true) {
            string anim = Random.Range(0f, 1f) > .5f ? "titleBulge" : "titleShake";
            GetComponent<Animation>().Play(anim);
            yield return new WaitForSeconds(Random.Range(6f, 10f));
        }
    }

    IEnumerator ColorizeMe() {
        yield return new WaitForSeconds(5f);
        while (true) {
            GoTweenConfig cfg = new GoTweenConfig();
            cfg.setEaseType(ease);
            cfg.colorProp("color", colors[Random.Range(0, colors.Length)]);
            Go.to(title, 2f + Random.Range(0f, 2f), cfg);
            
            yield return new WaitForSeconds(4f);
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
