﻿using UnityEngine;
using System.Collections;

public class PulseRing : MonoBehaviour {
	public float startScale, endScale;
	public float speed = 1f;
	public tk2dSprite sprite;
	private Vector3 endScaleVector;
	private Color endColor;
	private float timeScale = 2f;
	// Use this for initialization
	void Start () {
		endScaleVector = new Vector3(endScale,endScale,1f);
		endColor = new Color(sprite.color.r,sprite.color.g,sprite.color.b,0f);
		//Pulse (sprite.color, transform);
	}
	
	public void Pulse(PoolData pd) {
		transform.position = pd.t;
		sprite.color = pd.c;
		endColor = new Color(pd.c.r,pd.c.g,pd.c.b,0f);
		StartCoroutine("Go");
	}
	
	IEnumerator Go() {
		while(true) {
			transform.localScale = Vector3.Lerp(transform.localScale,endScaleVector,Time.deltaTime * speed);
			sprite.color = Color.Lerp(sprite.color,endColor,Time.deltaTime * timeScale);
			if(sprite.color.a < .05f)
				break;
			yield return 0;
		}
		PulsePoolLoader.pooler.PoolPulse(gameObject);
	}
	
	//PoolMe()
}
