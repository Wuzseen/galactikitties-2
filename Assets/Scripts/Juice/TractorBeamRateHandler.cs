﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TractorBeamRateHandler : MonoBehaviour {
	public Image sprite;
	public YarnBall theYarn;
	public ParticleSystem tractorSys;
	public Color offColor;
	public AnimationCurve onCurve, offCurve;
	public float maxEmissionRate = 300f;
	// Use this for initialization
	void Start () {
		sprite.fillAmount = 0f;
	}

	IEnumerator GoOn(float timeLimit) {
		StopCoroutine("GoOff");
		tractorSys.startColor = offColor;
		float time = 0f;
		while(time < timeLimit) {
			if(theYarn.go && !theYarn.paused)
				time += Time.deltaTime;
			float percent = time/timeLimit;
			tractorSys.emissionRate = onCurve.Evaluate(percent) * maxEmissionRate;
			sprite.fillAmount = percent;
			yield return 0;
		}
	}

	IEnumerator GoOff(float timeLimit) {
		StopCoroutine("GoOn");
		tractorSys.startColor = new Color(81,171,201);
		float startingTime = timeLimit;
		while(timeLimit > 0) {
			if(YarnRegister.ball.go && !YarnRegister.ball.paused)
				timeLimit -= Time.deltaTime;
			float percent = timeLimit/startingTime;
			tractorSys.emissionRate = onCurve.Evaluate(percent) * maxEmissionRate;
			sprite.fillAmount = percent;
			yield return 0;
		}
	}
	
	public void StartOn (float t) {
		StartCoroutine("GoOn",t);
	}
	
	public void StartOff (float t) {
		StartCoroutine("GoOff",t);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
