﻿using UnityEngine;
using System.Collections;

public class AchieveBars : MonoBehaviour {
	public UIProgressBar achieveProgress;
	public string achievement;
	// Use this for initialization
	void Start () {
		StartCoroutine(floatTween( (float)Achievements.Instance.getAchieveRequirement(achievement), Achievements.Instance.getAchieveCurrent(achievement)));
	}
	
	// Update is called once per frame
	void Update () {
			
	}
	IEnumerator floatTween( float maxValue, float currentValue){
		float newValue = 0.0f;
		while(newValue < currentValue) {
			newValue ++;
			float slideVal = (newValue/maxValue);
			achieveProgress.value = slideVal;
			yield return 0;
		}
		
	}
	
}
