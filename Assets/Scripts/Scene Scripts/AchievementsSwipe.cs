﻿using UnityEngine;
using System.Collections;

public class AchievementsSwipe : MonoBehaviour {
	public GameObject group1, group2;
	public bool increase;
	public int activegroup = 1;
	public float speed = 0.4f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(group1.transform.position.x < -.5f){
			activegroup = 2;
		}
		else{
			activegroup = 1;
		}
	}
	void OnClick () {
		if(increase && activegroup == 1)
			StartCoroutine(switchGroups(2));
		else if(!increase && activegroup == 2)
			StartCoroutine(switchGroups(1));
	}
	IEnumerator switchGroups(int next){
		if(next == 1){
			TweenPosition.Begin (group1, speed, new Vector2(-15f, 0f));
			TweenPosition.Begin (group2, speed, new Vector2(1507.5f, 0f));
		}
		else if(next == 2){
			TweenPosition.Begin (group2, speed, new Vector2(-15f, 0f));
			TweenPosition.Begin (group1, speed, new Vector2(-1515f, 0f));
		}
		yield return 0;
	}
}
