﻿using UnityEngine;
using System.Collections;

public class CloseMenuPanel : MonoBehaviour {
	public bool disableThis;
	public Collider coll;
	public GameObject otherPlanel, thisPanel;
	public TweenScale tweenOut, tweenIn;
	
	// Use this for initialization
	void OnPress() {
		if(disableThis)
			otherPlanel.SetActive(true);
		else
			coll.enabled = true;
		tweenOut.enabled = true;
		tweenIn.enabled = false;
		tweenOut.ResetToBeginning();
	}
	
	void OnClosePlay() {
		thisPanel.SetActive(false);	
	}
}
