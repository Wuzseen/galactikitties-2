﻿using UnityEngine;
using System.Collections;

public class CreateUser : MonoBehaviour {
	public UIInput username, password;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(LevelReg.lvlObj.loggedIn) {
			Application.LoadLevel("mainMenu");	
		}
	
	}
	void OnPress() {
#if UNITY_WP8
		Application.LoadLevel("mainMenu");
#else
		StartCoroutine(LevelReg.lvlObj.submitData(username.value, password.value));
#endif
	}
}
