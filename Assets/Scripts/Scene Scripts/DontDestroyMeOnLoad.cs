﻿using UnityEngine;
using System.Collections;

public class DontDestroyMeOnLoad : MonoBehaviour {
	// Should be used only for debugging quick things...
	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
