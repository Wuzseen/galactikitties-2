﻿using UnityEngine;
using System.Collections;

public class EndGameAchieve : MonoBehaviour {
	public UILabel textLabel;
	public UILabel scoreLabel;
	public UIProgressBar progress;
	private string achieveName;
	private int achieveNum;
	public int whichAchieve;
	public EndGameLoad endgame;
	// Use this for initialization
	void Start () {
		achieveNum = endgame.numbers[whichAchieve];
		switch(achieveNum){
		case 1:
			achieveName = "ClassicCats";
			break;
		case 2:
			achieveName = "ClassicScore";
			break;
		case 3:
			achieveName = "TimeCats";
			break;
		case 4:
			achieveName = "TimeScore";
			break;
		case 5:
			achieveName = "SurvivalTime";
			break;
		}
		scoreLabel.text = Achievements.Instance.getAchieveCurrent(achieveName) + "/" + Achievements.Instance.getAchieveRequirement(achieveName);
		textLabel.text = achieveName;
		StartCoroutine(floatTween( (float)Achievements.Instance.getAchieveRequirement(achieveName), Achievements.Instance.getAchieveCurrent(achieveName)));
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator floatTween( float maxValue, float currentValue){
		float newValue = 0.0f;
		while(newValue < currentValue) {
			newValue ++;
			float slideVal = (newValue/maxValue);
			progress.value = slideVal;
			yield return 0;
		}
		
	}
}
