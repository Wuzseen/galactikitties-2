﻿using UnityEngine;
using System.Collections;

public class EndGameLoad : MonoBehaviour {
	public int[] numbers = new int[5];
	// Use this for initialization
	void Awake () {
		for(int i = 0; i < numbers.Length; i++){
			numbers[i] = i+1;
		}
		for (int i = 0; i < numbers.Length; i++) {
			int temp = numbers[i];
			int randomIndex = Random.Range(i, numbers.Length);
			numbers[i] = numbers[randomIndex];
			numbers[randomIndex] = temp;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
