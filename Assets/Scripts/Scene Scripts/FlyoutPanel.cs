﻿using UnityEngine;
using System.Collections;

public class FlyoutPanel : MonoBehaviour {
	public bool disableThis;
	public GameObject panel;
	public TweenScale flyinTween, flyoutTween;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnPress(bool isDown) {
		if(!panel.activeSelf) {
			panel.SetActive(true);	
			flyinTween.enabled = true;
			flyoutTween.enabled = false;
			flyinTween.ResetToBeginning();
		}
	}
}
