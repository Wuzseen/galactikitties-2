﻿using UnityEngine;
using System.Collections;

public class HighScoreButton : MonoBehaviour {
	public UILabel score1Num, score2Num, score3Num, score4Num, score5Num;
	public UILabel score1Name, score2Name, score3Name, score4Name, score5Name;
	// Use this for initialization
	void Start () {
		StartCoroutine(LevelReg.lvlObj.GetScores());
	}
	
	// Update is called once per frame
	void Update () {
		if(this.enabled == true && LevelReg.lvlObj.scoresSet){
			score1Num.text = LevelReg.lvlObj.score1Num;
			score2Num.text = LevelReg.lvlObj.score2Num;
			score3Num.text = LevelReg.lvlObj.score3Num;	
			score4Num.text = LevelReg.lvlObj.score4Num;	
			score5Num.text = LevelReg.lvlObj.score5Num;
			score1Name.text = LevelReg.lvlObj.score1Name;
			score2Name.text = LevelReg.lvlObj.score2Name;
			score3Name.text = LevelReg.lvlObj.score3Name;
			score4Name.text = LevelReg.lvlObj.score4Name;
			score5Name.text = LevelReg.lvlObj.score5Name;

			LevelReg.lvlObj.scoresSet = false;
			}
	}
	
}
