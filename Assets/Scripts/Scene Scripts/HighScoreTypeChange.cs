﻿using UnityEngine;
using System.Collections;

public class HighScoreTypeChange : MonoBehaviour {
	public bool nextType = false;
	public HighScoreLabels script;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnClick() {
		if(nextType){
			script.getNextScoreType();
		}
		else{
			script.getPreviousScoreType();
		}
	}
}
