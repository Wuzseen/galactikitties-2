﻿using UnityEngine;
using System.Collections;

public class HowToButton : MonoBehaviour {
	public bool disableThis;
	public GameObject howToPanel, flyout;
	public TweenScale flyinTween, flyoutTween;
	// Use this for initialization
	void Start () {
	
	}
	
	void OnPress (bool isDown) {
		if(!isDown) {
			flyout.SetActive(true);
			flyinTween.enabled = true;
			flyoutTween.enabled = false;
			flyinTween.ResetToBeginning();
		}
	}
	
	void OnPlayTween() {
		if(disableThis)
			howToPanel.SetActive(false);
		else
			GetComponent<Collider>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
