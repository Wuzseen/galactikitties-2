﻿using UnityEngine;
using System.Collections;

public class LogIn : MonoBehaviour {
	public UIInput username;
	public UIInput password;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	if(LevelReg.lvlObj.loggedIn) {
			Application.LoadLevel("mainMenu");	
		}
	}
	
	void OnPress() {
		StartCoroutine(LevelReg.lvlObj.checkData(username.value, password.value));
	}
}
