﻿using UnityEngine;
using System.Collections;

public class LogOutButton : MonoBehaviour {
	public UILabel logType;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(LevelReg.lvlObj.loggedIn){
			logType.text = "Log Out";
		}
		else {
			logType.text = "Log In";		
		}
	}
	
	void OnPress() {
		if(LevelReg.lvlObj.loggedIn){
			PlayerPrefs.DeleteKey("UserName");
			PlayerPrefs.DeleteKey("Password");
			LevelReg.lvlObj.loggedUser = "";
			LevelReg.lvlObj.loggedIn = false;
		}
		else {
			
		}
	}
}
