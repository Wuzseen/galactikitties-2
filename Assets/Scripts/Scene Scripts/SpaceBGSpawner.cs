﻿using UnityEngine;
using System.Collections;

public class SpaceBGSpawner : MonoBehaviour {
	private static SpaceBGSpawner inst;
	// Use this for initialization
	void Start () {
		if(inst != null) {
			Destroy(this.gameObject);
			return;
		}
		inst = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
