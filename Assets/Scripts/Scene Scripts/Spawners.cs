﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnSide {
	public Vector3 first, second;

	public Vector3 RandomPoint() {
		return Vector3.Lerp(first,second,Random.Range(0f,1f)); // random point on the side
	}

	public SpawnSide(Vector3 fir, Vector3 sec) {
		this.first = fir;
		this.second = sec;
	}
}

public class Spawners : MonoBehaviour {
	private SpawnSide left,right,top,bottom;
	
	private float offset = 30f;
	public List<SpawnSide> spawnSides = new List<SpawnSide>();
	private static Spawners spawners;
	public static Spawners Instance {
		get { return spawners; }
	}
	// Use this for initialization
	void Awake () {
		if(spawners != null) {
			Destroy(this.gameObject);
			return;
		}
		// top right is 1,1. bot left is 0,0
		float z = this.transform.position.z;
		Vector3 worldTopRight = Camera.main.ViewportToWorldPoint(Vector3.one);
		worldTopRight += new Vector3(offset,offset);
		worldTopRight.z = z;
		Vector3 worldBottomLeft = Camera.main.ViewportToWorldPoint(Vector3.zero);
		worldBottomLeft += new Vector3(-offset,-offset);
		worldBottomLeft.z = z;
		Vector3 worldTopLeft = new Vector3(worldBottomLeft.x, worldTopRight.y);
		worldTopLeft += new Vector3(-offset,offset);
		worldTopLeft.z = z;
		Vector3 worldBottomRight = new Vector3(worldTopRight.x, worldBottomLeft.y);
		worldBottomRight += new Vector3(offset,-offset);
		worldBottomRight.z = z;
		spawnSides.Add(new SpawnSide(worldBottomLeft, worldTopLeft));
		spawnSides.Add(new SpawnSide(worldBottomRight, worldTopRight));
		spawnSides.Add(new SpawnSide(worldTopLeft, worldTopRight));
		spawnSides.Add(new SpawnSide(worldBottomLeft, worldBottomRight));
		spawners = this;
	}
	
	public SpawnSide RandomSpawnSide() {
		return spawnSides[Random.Range (0,spawnSides.Count)];	
	}
}
