﻿using UnityEngine;
using System.Collections;

public class StandardUIButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
        UIButton thisButton = this.GetComponent<UIButton>();
        thisButton.hover = new Color(61f, 51f, 255f, 255f);
        thisButton.pressed = new Color(61f, 51f, 255f, 255f);
	}
}
