﻿using UnityEngine;
using System.Collections;

public class TutorialPrompt : MonoBehaviour {
	public string playerPrefKey;
	public string prompt;
	public UILabel label;
	public UISprite background;
	private bool playing = false;

	// Use this for initialization
	void Start () {
		//PlayerPrefs.DeleteAll();
		if(!PlayerPrefs.HasKey("tutorial")) {
			PlayerPrefs.SetString("tutorial",playerPrefKey);
			StartCoroutine("Play");
		} else {
			string tutorialStrings = PlayerPrefs.GetString("tutorial");
			if(tutorialStrings.Contains(playerPrefKey)) {
				Clear ();
			} else {
				tutorialStrings = tutorialStrings + "," + playerPrefKey; // Comma is unnecessary but just seems cleaner
				PlayerPrefs.SetString("tutorial",tutorialStrings);
			}
		}
		if(PlayerPrefs.HasKey("tutorial_" + playerPrefKey)) {
			Clear ();
		} else {
			PlayerPrefs.SetInt("tutorial_" + playerPrefKey,1); // If this key exists, don't display the tutorial message.
			StartCoroutine("Play");
		}
	}

	public static void ClearTutorialPrefs() {
		//Re-enables the tutorial
		PlayerPrefs.DeleteKey("tutorial");
	}

	public void ReActivate() {
		ClearTutorialPrefs();
		label.text = "";
		Color c = background.color;
		c.a = 255f;
		background.color = c;
		if(!playing)
			StartCoroutine("Play");
	}

	IEnumerator Play() {
		playing = true;
		int count = prompt.Length;
		float timeLimit = count * .04f;
		float time = 0.0f;
		while(time <= timeLimit) {
			int numofchars = (int)((float)count * (time/timeLimit));
			time += Time.deltaTime;
			label.text = this.prompt.Substring(0,numofchars);
			yield return 0;
		}
		label.text = prompt;
	}
	
	public void Clear() {
		label.text = "";
		Color c = background.color;
		c.a = 0f;
		background.color = c;
		playing = false;
	}
}
