using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class hiScore : MonoBehaviour { 
	public UILabel scoreLbl, newScoreLabel;
	public int debugScore;
	public int mode; // 0 = classic, 1 = survival
	private bool newHighScore = false;
	private levelObj LevelReg.lvlObj;
	// Use this for initialization
	void Start () {
		if(LevelReg.lvlObj != null) {
			debugScore = LevelReg.lvlObj.getScore();
		}
		if(GoogleAnalytics.instance) {
			GoogleAnalytics.instance.LogScreen("HiScore");
		}
		scoreLbl.text = debugScore.ToString();
		AddScore();
	}

	string[] scoreGather() {
		return PlayerPrefs.GetString("_scores").Split(',');	
	}
	
	void AddScore() {
		//PlayerPrefs.SetString("_scores","");
		string[] sArr;
		if(LevelReg.lvlObj.getMode() == 0){
			if(PlayerPrefs.HasKey("_classicScores")) {
				sArr = PlayerPrefs.GetString("_classicScores").Split(',');
				print (PlayerPrefs.GetString("_classicScores"));
			}
			else {
				PlayerPrefs.SetString("_classicScores","");
				sArr = new string[0];
				print (":(");
			}
		}
		else if(LevelReg.lvlObj.getMode() == 1){
			if(PlayerPrefs.HasKey("_timeScores")) {
				sArr = PlayerPrefs.GetString("_timeScores").Split(',');
				print (PlayerPrefs.GetString("_timeScores"));
			}
			else {
				PlayerPrefs.SetString("_timeScores","");
				sArr = new string[0];
				print (":(");
			}
		}
		else if(LevelReg.lvlObj.getMode() == 2){
			if(PlayerPrefs.HasKey("_survivalScores")) {
				sArr = PlayerPrefs.GetString("_survivalScores").Split(',');
				print (PlayerPrefs.GetString("_survivalScores"));
			}
			else {
				PlayerPrefs.SetString("_survivalScores","");
				sArr = new string[0];
				print (":(");
			}
		}
		else {
			PlayerPrefs.SetString("_scores","");
			sArr = new string[0];
			print (":(");
		}
		string playerPrefVal = "";
		List<int> scores = new List<int>();
		foreach(string s in sArr) {
			scores.Add(int.Parse(s));
		}
		scores.Add(debugScore);
		while(scores.Count < 5) {
			scores.Add(0);
		}
		scores.Sort();
		scores.Reverse();
		for(int i = 0; i < 5; i++) {
			if(scores[i] == debugScore) {
				newScoreLabel.gameObject.SetActive(true);
//				newScoreLabel.animation.Play();
			}
			playerPrefVal += scores[i].ToString();
			if(i < 4) {
				playerPrefVal += ",";
			}
		}
//		foreach(string s in sArr) {
//			if(s == "")
//				continue;
//			int tScore = int.Parse(s);
//			if(debugScore > tScore) {
//				newHighScore = true;
//				scoreAdded = true;
//				string firstChar = playerPrefVal == "" ? "" : ",";
//				playerPrefVal += firstChar + debugScore.ToString();
//				scoreCount++;
//			}
//			if(scoreCount >= 5)
//				break;
//			else if(scoreAdded)
//				playerPrefVal += ",";
//			playerPrefVal += sArr;
//			scoreCount++;
//			print ("PLAYERPREF: " + playerPrefVal);
//		}
//		if(sArr.Length < 2) {// Hasn't ever been set...
//			if(LevelReg.lvlObj.getMode() == 0){
//				PlayerPrefs.SetString("_classicScores", debugScore.ToString());
//			}
//			else if(LevelReg.lvlObj.getMode() == 1){
//				PlayerPrefs.SetString("_timeScores", debugScore.ToString());
//			}
//			else if(LevelReg.lvlObj.getMode () == 2){
//				PlayerPrefs.SetString("_survivalScores", debugScore.ToString());
//			}
//		}
//		else if(!scoreAdded && sArr.Length < 5) { // There haven't been 5 registered scores yet
//			playerPrefVal += ',' + debugScore.ToString();
			if(LevelReg.lvlObj.getMode() == 0){
				PlayerPrefs.SetString("_classicScores",playerPrefVal);
			}
			else if(LevelReg.lvlObj.getMode() == 1){
				PlayerPrefs.SetString("_timeScores",playerPrefVal);
			}
			else if(LevelReg.lvlObj.getMode () == 2){
				PlayerPrefs.SetString("_survivalScores", playerPrefVal);
			}
			else {
				PlayerPrefs.SetString("_scores", playerPrefVal);
			}
//		}
		PlayerPrefs.Save();
	}
}
