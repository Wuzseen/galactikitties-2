using UnityEngine;
using System.Collections;

public class instruction : MonoBehaviour {
	public string nextScene, previousScene;
	public Texture2D bg;
	public GUIStyle leftArrow, rightArrow, mainStyle;
	private float scale;
	// Use this for initialization
	void Start () {
		scale = .4f;
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnGUI () {
		GUI.DrawTexture(new Rect(0,0, Screen.width,Screen.height), bg);
		if(GUI.Button(new Rect(Screen.width/2 - 375/2,Screen.height - 125 ,1757f * .21f,387f * .21f), "", mainStyle))
			Application.LoadLevel("mainMenu");
		if(GUI.Button (new Rect(Screen.width - 200, Screen.height - 150,330 * scale, 293 * scale), "", rightArrow))
			Application.LoadLevel(nextScene);
		if(GUI.Button (new Rect(50, Screen.height - 150,330 * scale, 293 * scale), "", leftArrow))
			Application.LoadLevel(previousScene);
	}
}
