using UnityEngine;
using System.Collections;

public class loadSceneObj : MonoBehaviour {
	public float startSize;
	private static loadSceneObj inst;
	
	// Use this for initialization
	void Start () {
		if(inst != null) {
			Destroy(this.gameObject);
			return;
		}
		inst = this;
		startSize = Camera.main.orthographicSize;
		DontDestroyOnLoad(this);
		
		Application.LoadLevel("mainMenu");
		
		/*if(PlayerPrefs.HasKey("UserName")){
			Application.LoadLevel("LoginScreen");
			StartCoroutine(LevelReg.lvlObj.checkData(PlayerPrefs.GetString("UserName"), PlayerPrefs.GetString ("Password")));
		}
		else{
			Application.LoadLevel("LoginScreen");	
		}*/
	}
	
	// Update is called once per frame
	void Update () {
		if(Application.loadedLevelName == "highScore")
			Camera.main.orthographicSize = 5f;
		else
			Camera.main.orthographicSize = startSize;
	}
}
