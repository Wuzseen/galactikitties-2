#warning Upgrade NOTE: unity_Scale shader variable was removed; replaced 'unity_Scale.w' with '1.0'

// Shader created with Shader Forge Beta 0.22 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.22;sub:START;pass:START;ps:lgpr:1,nrmq:0,limd:0,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,uamb:True,mssp:True,ufog:True,aust:True,igpj:True,qofs:0,lico:1,qpre:3,flbk:tk2d/CutoutVertexColor,rntp:2,lmpd:False,lprd:True,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0;n:type:ShaderForge.SFN_Final,id:1,x:31696,y:32397|normal-9271-RGB,emission-9286-OUT,transm-133-OUT,lwrap-133-OUT,alpha-9271-A;n:type:ShaderForge.SFN_Subtract,id:18,x:33624,y:32681|A-22-OUT,B-19-OUT;n:type:ShaderForge.SFN_Vector1,id:19,x:33793,y:32752,v1:0.5;n:type:ShaderForge.SFN_Abs,id:21,x:33453,y:32681|IN-18-OUT;n:type:ShaderForge.SFN_Frac,id:22,x:33793,y:32627|IN-24-OUT;n:type:ShaderForge.SFN_Panner,id:23,x:34133,y:32627,spu:0.25,spv:0;n:type:ShaderForge.SFN_ComponentMask,id:24,x:33964,y:32627,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-23-UVOUT;n:type:ShaderForge.SFN_Multiply,id:25,x:33269,y:32681,cmnt:Triangle Wave|A-21-OUT,B-26-OUT;n:type:ShaderForge.SFN_Vector1,id:26,x:33453,y:32809,v1:2;n:type:ShaderForge.SFN_Power,id:133,x:33100,y:32681,cmnt:Panning gradient|VAL-25-OUT,EXP-8547-OUT;n:type:ShaderForge.SFN_Multiply,id:166,x:32908,y:32498,cmnt:Glow|A-168-RGB,B-8677-OUT,C-133-OUT;n:type:ShaderForge.SFN_Color,id:168,x:33100,y:32412,ptlb:Glow Color,c1:0,c2:0.8308824,c3:0.280781,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:8547,x:33269,y:32833,ptlb:Bulge Shape,v1:5;n:type:ShaderForge.SFN_ValueProperty,id:8677,x:33100,y:32584,ptlb:Glow Intensity,v1:1.2;n:type:ShaderForge.SFN_Tex2d,id:9271,x:32652,y:32663,ptlb:node_9271,tex:822be9994360c604088a48da3131903b,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Add,id:9286,x:32478,y:32527|A-166-OUT,B-9271-RGB;proporder:168-8547-8677-9271;pass:END;sub:END;*/

Shader "Shader Forge/animate" {
    Properties {
        _GlowColor ("Glow Color", Color) = (0,0.8308824,0.280781,1)
        _BulgeShape ("Bulge Shape", Float ) = 5
        _GlowIntensity ("Glow Intensity", Float ) = 1.2
        _node9271 ("node_9271", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _GlowColor;
            uniform float _BulgeShape;
            uniform float _GlowIntensity;
            uniform sampler2D _node9271; uniform float4 _node9271_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float3 tangentDir : TEXCOORD2;
                float3 binormalDir : TEXCOORD3;
                float3 shLight : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.shLight = ShadeSH9(float4(v.normal * 1.0,1)) * 0.5;
                o.normalDir = mul(float4(v.normal,0), _World2Object).xyz;
                o.tangentDir = normalize( mul( _Object2World, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.binormalDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                float3x3 tangentTransform = float3x3( i.tangentDir, i.binormalDir, i.normalDir);
/////// Normals:
                float2 node_9373 = i.uv0;
                float4 node_9271 = tex2D(_node9271,TRANSFORM_TEX(node_9373.rg, _node9271));
                float3 normalLocal = node_9271.rgb;
                float3 normalDirection =  mul( normalLocal, tangentTransform ); // Perturbed normals
////// Lighting:
////// Emissive:
                float4 node_9374 = _Time + _TimeEditor;
                float node_133 = pow((abs((frac((node_9373.rg+node_9374.g*float2(0.25,0)).r)-0.5))*2.0),_BulgeShape);
                float3 emissive = ((_GlowColor.rgb*_GlowIntensity*node_133)+node_9271.rgb);
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,node_9271.a);
            }
            ENDCG
        }
    }
    FallBack "tk2d/CutoutVertexColor"
    CustomEditor "ShaderForgeMaterialInspector"
}
