// Shader created with Shader Forge Beta 0.22 
// Shader Forge (c) Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:0.22;sub:START;pass:START;ps:lgpr:1,nrmq:1,limd:0,blpr:1,bsrc:3,bdst:7,culm:0,dpts:2,wrdp:False,uamb:True,mssp:True,ufog:True,aust:True,igpj:True,qofs:0,lico:1,qpre:3,flbk:,rntp:2,lmpd:False,lprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,hqsc:True,hqlp:False,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.1280277,fgcg:0.1953466,fgcb:0.2352941,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0;n:type:ShaderForge.SFN_Final,id:1,x:32719,y:32712|emission-9115-OUT,alpha-9105-A,olwid-8713-OUT;n:type:ShaderForge.SFN_Vector1,id:8713,x:33262,y:32984,v1:3;n:type:ShaderForge.SFN_Time,id:8947,x:33840,y:32607;n:type:ShaderForge.SFN_Cos,id:9080,x:33583,y:32695|IN-8947-TTR;n:type:ShaderForge.SFN_Tex2d,id:9105,x:33462,y:32559,ptlb:node_9105,tex:822be9994360c604088a48da3131903b,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Color,id:9114,x:33400,y:32830,ptlb:node_9114,c1:0,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:9115,x:33180,y:32790|A-9105-RGB,B-9114-RGB;proporder:9105-9114;pass:END;sub:END;*/

Shader "Shader Forge/outlineBuff" {
    Properties {
        _node9105 ("node_9105", 2D) = "white" {}
        _node9114 ("node_9114", Color) = (0,0.5,0.5,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.pos = mul(UNITY_MATRIX_MVP, float4(v.vertex.xyz + v.normal*3.0,1));
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                return fixed4(float3(0,0,0),0);
            }
            ENDCG
        }
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers xbox360 ps3 flash 
            #pragma target 3.0
            uniform sampler2D _node9105; uniform float4 _node9105_ST;
            uniform float4 _node9114;
            struct VertexInput {
                float4 vertex : POSITION;
                float4 uv0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o;
                o.uv0 = v.uv0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float2 node_9134 = i.uv0;
                float4 node_9105 = tex2D(_node9105,TRANSFORM_TEX(node_9134.rg, _node9105));
                float3 emissive = (node_9105.rgb*_node9114.rgb);
                float3 finalColor = emissive;
/// Final Color:
                return fixed4(finalColor,node_9105.a);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
