﻿using UnityEngine;
using System.Collections;

public class UserLogged : MonoBehaviour {
	public UILabel username;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		username.text = LevelReg.lvlObj.loggedUser;
	}
}
